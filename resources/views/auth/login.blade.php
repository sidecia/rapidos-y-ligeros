@extends('layouts.app')

@section('title','Bienvenido')

@section('content')
<main class="py-4 login_admin">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-xs-12">
                <div class="card __body">
                    <div class="card-header __titulo">
                        {{ __(' Rápidos y Ligeros ') }}
                         <img src="{{ asset('images/logo.png') }}" alt="tag" class="logo_login">
                    </div>
                    
                    @if ($errors->has('global'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('global') }}</strong>
                        </span>
                    @endif
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="usuario" class="col-sm-3 col-form-label text-md-right">{{ __('Usuario') }}</label>

                                <div class="col-md-7">
                                    <input id="usuario" type="text" class="form-control {{ $errors->has('usuario') ? ' is-invalid' : '' }}" name="usuario" value="{{ old('usuario') }}" required autofocus>

                                    @if ($errors->has('usuario'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('usuario') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-7">
                                    <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

    
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn_login ">
                                        {{ __('Iniciar Sesión') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
