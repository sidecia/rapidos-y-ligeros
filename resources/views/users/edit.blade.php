@extends('layouts.app')

@section('title','Editar Usuarios')

@section('content')
	<div class="container">
		<h1>Editar Usuario</h1>
		@if (session()->has('info'))
			 <div class="alert alert-success">
			 	 {{ session('info') }}	
			 </div>
			
		@endif
		<form method="POST" action="{{ route('usuarios.update', $usuario->id) }}">
			{!! method_field('PUT') !!}
			
			@include('users.form')
			<button type="submit" class="btn btn-info btn-xs">Guardar</button>
		</form>
	</div>
@endsection