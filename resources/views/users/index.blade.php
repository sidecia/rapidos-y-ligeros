@extends('layouts.app')

@section('title','Usuarios')

@section('content')
	<div class="container __listados_general">
		<h3>Usuarios</h3>
		<div class="__acciones_buttons btn-group">
			<a href="{{ route('usuarios.create') }}" class="btn btn-default btn-outline-dark">Nuevo Usuario</a>
		</div>
		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Nombre</th>
		      <th scope="col">Usuario</th>
		      <th scope="col">Email</th>
		      <th scope="col">Role</th>
		      <th scope="col"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($users as $user)
				<tr>
			      <th scope="row">{{ $loop->iteration }}</th>
			      <td>{{ $user->nombre }} {{ $user->a_materno }} {{ $user->a_paterno }}</td>
			      <td>{{ $user->usuario }}</td>
			      <td>{{ $user->email }}</td>
			      <td>
			      	{{ $user->tipo_usuario }}
			      	
			      </td>
			      <td>
			      	<div class="btn-group __acciones_buttons_grupo">
						<a class="btn btn-default btn-outline-dark __accion_registro" href="{{ route('usuarios.edit', $user->id) }}">
								<i class="el el-pencil"></i> Editar
						</a>
						<div class="dropdown __accion_registro">
						  <button class="btn btn-default btn-outline-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Más
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						    <form style = "display: inline;" method = 'POST'  action = "{{ route('usuarios.destroy', $user->id) }}" >
						  		{!! csrf_field() !!}
						  		{!! method_field('DELETE') !!}
						  		<button class="dropdown-item" type="submit">
						  			<i class="el el-remove text-danger"></i> <span class="text-danger">Eliminar</span>
						  		</button>
						  	</form>
						  </div>
						</div>
					</div>
			      </td>	
			    </tr>

		  	@endforeach
		    
		  </tbody>
		</table>
	</div>
@endsection