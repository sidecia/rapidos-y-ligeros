{!! csrf_field() !!}
<div class="form-row">
    <div class="form-group col-md-4">
		<label class=" custom-label-class" for="nombre">
			Nombre
			<input type="text" name="nombre" class="form-control" value="{{ old('nombre',$usuario->nombre ?? '') }}">
				{!! $errors->first('nombre','<span class=error>:message</span>') !!}
		</label>
    </div>
	<div class="form-group col-md-4">
		<label class=" custom-label-class" for="a_paterno">
			Apellido Paterno 
			<input type="text" name="a_paterno" class="form-control" value="{{ old('a_paterno',$usuario->a_paterno ?? '') }}">
			{!! $errors->first('a_paterno','<span class=error>:message</span>') !!}
		</label>
    </div>
	<div class="form-group col-md-4">
		<label for="name">
			Apellido Materno 
			<input type="text" name="a_materno" class="form-control" value="{{ old('a_materno',$usuario->a_materno ?? '') }}">
			{!! $errors->first('a_materno','<span class=error>:message</span>') !!}
		</label>
    </div>
	<div class="form-group col-md-4">
			<label for="usuario">
				Usuario 
				<input type="text" name="usuario" class="form-control" value="{{  old('usuario',$usuario->usuario ?? '') }}">
				{!! $errors->first('usuario','<span class=error>:message</span>') !!}
			</label>
    </div>
	
	@unless($usuario->id)
	<div class="form-group col-md-4">
		<label for="password">
			Password  
			<input type="password" name="password" class="form-control">
			{!! $errors->first('password','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="password_confirmation">
			Password Confirm 
			<input type="password" name="password_confirmation" class="form-control">
			{!! $errors->first('password_confirmation','<span class=error>:message</span>') !!}
		</label>
	</div>
	@endunless
	<div class="form-group col-md-4">
			<label for="tipo_usuario">
				Tipo 
				<input type="text" name="tipo_usuario" class="form-control" value="{{  old('tipo_usuario',$usuario->tipo_usuario ?? '') }}">
				{!! $errors->first('tipo_usuario','<span class=error>:message</span>') !!}
			</label>
    </div>
	<div class="form-group col-md-4">
			<label for="puesto">
				Puesto 
				<input type="text" name="puesto" class="form-control" value="{{  old('puesto',$usuario->puesto ?? '') }}">
				{!! $errors->first('puesto','<span class=error>:message</span>') !!}
			</label>
    </div>
	<div class="form-group col-md-4">
			<label for="status">
				Status 
				<input type="text" name="status" class="form-control" value="{{  old('status',$usuario->status ?? '') }}">
				{!! $errors->first('status','<span class=error>:message</span>') !!}
			</label>
    </div>
	<div class="form-group col-md-4">
			<label for="administrador">
				Administrador 
				<input type="text" name="administrador" class="form-control" value="{{  old('administrador',$usuario->administrador ?? '') }}">
				{!! $errors->first('administrador','<span class=error>:message</span>') !!}
			</label>
    </div>
	<div class="form-group col-md-4">
		<label for="whatsapp">
		whatsapp 
				<input type="text" name="whatsapp" class="form-control" value="{{  old('whatsapp',$usuario->whatsapp ?? '') }}">
				{!! $errors->first('whatsapp','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="whatsapp_empresa">
		whatsapp empresa
				<input type="text" name="whatsapp_empresa" class="form-control" value="{{  old('whatsapp_empresa',$usuario->whatsapp_empresa ?? '') }}">
				{!! $errors->first('whatsapp_empresa','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="whatsappstring">
		whatsapp String
				<input type="text" name="whatsappstring" class="form-control" value="{{  old('whatsappstring',$usuario->whatsappstring ?? '') }}">
				{!! $errors->first('whatsappstring','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="correo">
				Email 
				<input type="text" name="correo" class="form-control" value="{{  old('correo',$usuario->correo ?? '') }}">
				{!! $errors->first('correo','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="correo_password">
				Email password
				<input type="text" name="correo_password" class="form-control" value="{{  old('correo_password',$usuario->correo_password ?? '') }}">
				{!! $errors->first('correo_password','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="correo_personal">
				Email personal
				<input type="text" name="correo_personal" class="form-control" value="{{  old('correo_personal',$usuario->correo_personal ?? '') }}">
				{!! $errors->first('correo_personal','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="correo_personal_ok">
				Email personal ok
				<input type="text" name="correo_personal_ok" class="form-control" value="{{  old('correo_personal_ok',$usuario->correo_personal_ok ?? '') }}">
				{!! $errors->first('correo_personal_ok','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="sexo">
				Sexo
				<input type="text" name="sexo" class="form-control" value="{{  old('sexo',$usuario->sexo ?? '') }}">
				{!! $errors->first('sexo','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="lada">
				Lada
				<input type="text" name="lada" class="form-control" value="{{  old('lada',$usuario->lada ?? '') }}">
				{!! $errors->first('lada','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="pais">
				País
				<input type="text" name="pais" class="form-control" value="{{  old('pais',$usuario->pais ?? '') }}">
				{!! $errors->first('pais','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="sucursal">
				Sucursal
				<input type="text" name="sucursal" class="form-control" value="{{  old('sucursal',$usuario->sucursal ?? '') }}">
				{!! $errors->first('sucursal','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="banco_clabe">
		banco_clabe
				<input type="text" name="banco_clabe" class="form-control" value="{{  old('banco_clabe',$usuario->banco_clabe ?? '') }}">
				{!! $errors->first('banco_clabe','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="banco">
		banco
				<input type="text" name="banco" class="form-control" value="{{  old('banco',$usuario->banco ?? '') }}">
				{!! $errors->first('banco','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="cuenta">
		Cuenta
				<input type="text" name="cuenta" class="form-control" value="{{  old('cuenta',$usuario->cuenta ?? '') }}">
				{!! $errors->first('cuenta','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="ciudad">
		Ciudad
				<input type="text" name="ciudad" class="form-control" value="{{  old('ciudad',$usuario->ciudad ?? '') }}">
				{!! $errors->first('ciudad','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="cedula_ok">
		Cedula
				<input type="text" name="cedula_ok" class="form-control" value="{{  old('cedula_ok',$usuario->cedula_ok ?? '') }}">
				{!! $errors->first('cedula_ok','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="ine_ok">
		INE
				<input type="text" name="ine_ok" class="form-control" value="{{  old('ine_ok',$usuario->ine_ok ?? '') }}">
				{!! $errors->first('ine_ok','<span class=error>:message</span>') !!}
		</label>
	</div>
	<div class="form-group col-md-4">
		<label for="ine_back_ok">
		INE Back
				<input type="text" name="ine_back_ok" class="form-control" value="{{  old('ine_back_ok',$usuario->ine_back_ok ?? '') }}">
				{!! $errors->first('ine_back_ok','<span class=error>:message</span>') !!}
		</label>
	</div>
</div>	
<hr>