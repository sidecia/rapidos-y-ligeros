@extends('layouts.app')

@section('title','Alta Usuarios')

@section('content')

<div class="container">
		<h1>Crear Usuario</h1>
		@if (session()->has('info'))
			 <div class="alert alert-success">
			 	 {{ session('info') }}	
			 </div>
		@endif
		<form method="POST" action="{{ route('usuarios.store') }}">
			@include('users.form',['usuario' => new App\User ])
			<button type="submit" class="btn btn-info btn-xs">Guardar</button>
		</form>
	</div>
@endsection