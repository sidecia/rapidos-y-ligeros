@extends('layouts.app')

@section('title', 'Mis Alimentos - Listado')

@section('content')
    <mis-alimentos :user="{{ $userAuth }}" :categorias='{{ $categorias }}'
        :lastfoodsvalidated='{{ $lastFoodsValid }}' :lastfoodspend='{{ $lastFoodsPend }}'
        :sincategoria='{{ $sinCategoria }}'>
    </mis-alimentos>
@endsection
