@extends('layouts.app')

@section('title', 'Agregar Alimentos')

@section('content')
    <alimento-create :categories='{{ json_encode($categories) }}' :foods='{{ json_encode($foods) }}'
        :ingredients='{{ json_encode($ingredients) }}' :measures='{{ json_encode($measures) }}'
        :product_presentation='{{ json_encode($product_presentation) }}'
        :supermarkets='{{ json_encode($supermarkets) }}'
        :ingredients_measures='{{ json_encode($ingredients_measures) }}'></alimento-create>
@endsection
