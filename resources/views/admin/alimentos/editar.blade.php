@extends('layouts.app')

@section('title', 'Editar Alimento')

@section('content')
    <alimento-edit :foodinfo='{{ json_encode($foodData) }}' :categories='{{ json_encode($categories) }}'
        :foods='{{ json_encode($foods) }}' :ingredients='{{ json_encode($ingredients) }}'
        :measures='{{ json_encode($measures) }}' :product_presentation='{{ json_encode($product_presentation) }}'
        :supermarkets='{{ json_encode($supermarkets) }}' :usuario='{{ json_encode($usuario) }}'
        :ingredients_measures='{{ json_encode($ingredients_measures) }}' :data_extra='{{ json_encode($dataExtra) }}'>
    </alimento-edit>
@endsection
