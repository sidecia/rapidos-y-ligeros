@extends('layouts.app')

@section('title', 'Mis Favoritos')

@section('content')
    <listado-alimentos-favoritos
        :user       =   "{{ $userAuth }}" 
		:alimentos = '{{ $foods }}'
    ></listado-alimentos-favoritos>
@endsection
