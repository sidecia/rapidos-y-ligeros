@extends('layouts.app')

@section('title', 'Resultados Alimentos')

@section('content')
    <listado-resultados-alimentos
        :user       =   "{{ $userAuth }}" 
		:alimentos = '{{ $foods }}'
        :buscarkey  = '{{ json_encode($buscar) }}'
    ></listado-resultados-alimentos>
@endsection
