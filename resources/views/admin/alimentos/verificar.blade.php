@extends('layouts.app')

@section('title', 'Verificar Alimentos')

@section('content')
    <alimento-verificar :user="{{ $userAuth }}" :nutriologas='{{ $nutriologas }}'
        :lastfoodsten="{{ $lastfoodsten }}" :rezagadosfoods="{{ $rezagadosfoods }}"></alimento-verificar>
@endsection
