@extends('layouts.app')

@section('title','Catalogo - Listado')

@section('content')
		<catalogo
		:user       =   "{{ $userAuth }}" 
		:categorias = '{{ $categorias }}'
		>	
		</catalogo>
@endsection