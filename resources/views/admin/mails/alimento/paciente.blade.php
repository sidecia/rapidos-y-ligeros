<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Documento sin título</title>
    <style type="text/css">
        @font-face {
            font-family: 'headoh';
            src: url('https://dietasi.com/rapidosyligeros/public/mail_images/font/HEADOH.TTF') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        #main_mail {
            margin: 0 auto;
            width: 600px;
        }

        td {
            text-align: center;
        }

        td.titulo_alim {
            text-transform: uppercase;
            color: #9E2E69;
            font-size: 27px;
            font-weight: bold;
        }

        td.tiutlo_precios {
            width: 205px;
            background-color: #30A094;
            color: #FFFEFF;
            font-weight: bold;
            border-radius: 5px;
        }

        td.precios {
            color: #30A094;
            font-weight: bold;
        }

        .bubble {
            position: relative;
            padding: 0px;
            background: #FFF001;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 5px;
        }

        .bubble:after {
            content: '';
            position: absolute;
            border-style: solid;
            border-width: 15px 15px 0;
            border-color: #FFF001 transparent;
            display: block;
            width: 0;
            z-index: 1;
            bottom: -15px;
            left: 235px;
        }

        #main_bubble {
            overflow: hidden;
            width: 450px;
            margin: 6px auto;
        }

        .bubble_img {
            float: left;
            padding: 0px 23px 0px 5px;
            margin: 0 auto;
        }

        .bubble_img img {
            width: 45px;
            height: 55px;
        }

        .bubble_txt {
            color: #9D6D00;
            float: left;
            width: 370px;
            word-break: break-all;
        }

        #table_info_nutri {
            margin: 30px auto;
            width: 490px;
        }

        .titulos_rosas {
            color: #EE8099;
            font-weight: bold;
            text-align: left;
            width: 225px;
        }

        .padd_td td {
            padding: 25px 0 0 0;
        }

        .txt_ifo_alim {
            color: #484848;
            text-align: justify;
            word-break: break-all;
            width: 225px;
        }

        .txt_ifo_alim div {
            width: 211px;
        }

        #table_kcal td {
            text-align: center;
        }

        #main_kcal {
            overflow: hidden;
            width: 220px;
            margin: 0 auto;
        }

        #main_kcal div {
            float: left;
        }

        #main_kcal img {
            width: 64px;
            height: 64px;
        }

        .titulo_kcal {
            color: #A12F6D;
            font-family: 'headoh';
            font-size: 48px;
            padding: 9px 0 0 0;
        }

        #table_nut_dice {
            width: 465px;
            margin: 40px auto;
        }

        #table_nut_dice td {
            text-align: left;
            word-break: break-all;
        }

        .nut_dice_img {
            text-align: center;
            width: 100px;
        }

        .txt_nut_dice {
            color: #8D8D8D;
        }

    </style>
</head>

<body>
    @foreach ($foods as $food)

        <table align="center" id="main_mail"
            style="font-family:Arial, Helvetica, sans-serif;margin:0 auto !important;width:600px !important;" border="0"
            cellspacing="0" cellpadding="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="height:450px;"><img style="height:450px"
                        src="https://dietasi.com/rapidosyligeros/public/mail_images/newsletter-templateMail2_01.png">
                </td>
            </tr>
            <tr>
                <td>
                    <table width="600" cellspacing="0" cellpadding="0" align="center" border="0">
                        <tr>
                            <td class="titulo_alim"
                                style="text-transform:uppercase;color:#9f2d6d;font-size:27px;font-weight:bold; padding:30px 0 30px 0 !important; text-align:center;"
                                colspan="2">{{ $food->name }}</td>
                        </tr>
                        <tr>
                            <td valign="middle"
                                style="padding: 0 25px 43px 0px !important;width: 260px;text-align: right;" rowspan="2">
                                <img style=" max-height:150px; max-width:210px; border:none;"
                                    src="https://dietasi.com/rapidosyligeros/public/{{ $food->image }}">
                            </td>
                            <td valign="middle" style=" width:205px;padding:0 0 43px 0 !important; text-align:center;">
                                <table width="205px" border="0">
                                    <tr>
                                        <td colspan="2"
                                            style=" text-align:center;width:205px;background-color:#30A094;color:#FFFEFF;font-weight:bold;border-radius: 5px;padding: 3px 0 3px 0; font-size:18px;"
                                            class="tiutlo_precios">Precio por:</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="color:#39b5ac; border-right:solid 2px #39b5ac;"
                                            class="precios"><span style="font-size:18px;">Unidad</span><br><span
                                                style="font-size:24px; font-weight:bold;">$ {{ $food->price }}</span>
                                        </td>
                                        <td align="center" style="color:#39b5ac;" class="precios"><span
                                                style="font-size:18px;">Caja</span><br><span
                                                style="font-size:24px; font-weight:bold;">$
                                                {{ $food->box_price }}</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"
                                            style="text-align:center;width:205px;background-color:#30A094;color:#FFFEFF;font-weight:bold;border-radius: 5px;padding: 3px 0 3px 0; font-size:18px;"
                                            class="tiutlo_precios">¿Donde comprar?</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="color:#39b5ac; font-size:18px;" align="center"
                                            class="precios">
                                            @foreach ($food->supermarkets as $supermercado)
                                                {{ $supermercado->name }},
                                            @endforeach

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td background="https://dietasi.com/rapidosyligeros/public/mail_images/newsletter-templateMail2_03.png"
                    style="height:129px !important;background-image:url(https://dietasi.com/rapidosyligeros/public/mail_images/newsletter-templateMail2_03.png);">
                    <div style="height:129px !important"></div>
                </td>
            </tr>
            <tr>
                <td background="https://dietasi.com/rapidosyligeros/public/mail_images/newsletter-templateMail2_04.png"
                    align="center"
                    style=" background:url(https://dietasi.com/rapidosyligeros/public/mail_images/newsletter-templateMail2_04.png) no-repeat;"
                    valign="top">
                    <table style="width:500px !important; margin:0 auto !important;" border="0" cellpadding="0"
                        cellspacing="0" align="center">
                        <tr>
                            <td align="center"
                                style="-webkit-border-radius: 10px;	-moz-border-radius: 10px;border-radius:5px;"
                                colspan="2">
                                <table width="488" style="border-radius:5px;" border="0" cellpadding="0"
                                    cellspacing="0">
                                    <tr>
                                        <td align="center" style="background: #FFF001;"><img
                                                style="border:none;width:55px;height:55px;"
                                                src="https://dietasi.com/rapidosyligeros/public/mail_images/iconoFoco.png?1.0">
                                        </td>
                                        <td
                                            style="color:#6d1c00;background: #FFF001; font-size:14px; text-align:left; width:390px; padding:4px 0 4px 0;">
                                            {{ $food->strategies }}</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center" colspan="2">
                                            <img style="display:block !important;"
                                                src="https://dietasi.com/rapidosyligeros/public/mail_images/triangulo.png">
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table
                                    style=" padding:15px 0 0 0 !important;margin:0px auto !important;width:490px !important;"
                                    id="table_info_nutri" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="color:#ef5b7f; font-size:18px;text-align:left;padding:0px 0 3px 0;width:225px;"
                                            class="titulos_rosas">Las Reglas</td>
                                        <td rowspan="4">
                                            <div style="width:60px;">&nbsp;</div>
                                        </td>
                                        <td style="color:#ef5b7f; font-size:18px;text-align:left;padding:0px 0 3px 0;width:225px;"
                                            class="titulos_rosas">Identifica los excesos</td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:14px;color:#323232;width:225px;" valign="top"
                                            class="txt_ifo_alim">
                                            <div style="width:220px !important;">{{ $food->rules }}</div>
                                        </td>
                                        <td style="font-size:14px;color:#323232;width:225px;" valign="top"
                                            class="txt_ifo_alim">
                                            <div style="width:220px !important;">{{ $food->exaggerate }}</div>
                                        </td>
                                    </tr>
                                    <tr class="padd_td">
                                        <td style="color:#ef5b7f; font-size:18px;padding:25px 0 3px 0;text-align:left;width:225px;"
                                            class="titulos_rosas">Los Riegos</td>
                                        <td style="color:#ef5b7f; font-size:18px;padding:25px 0 3px 0;text-align:left;width:225px;"
                                            class="titulos_rosas">Recomendación General</td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-size:14px;color:#323232;width:225px;"
                                            class="txt_ifo_alim">
                                            <div style="width:220px !important;">{{ $food->risks }}</div>
                                        </td>
                                        <td valign="top" style="font-size:14px;color:#323232;width:225px;"
                                            class="txt_ifo_alim">
                                            <div style="width:220px !important;">{{ $food->generals }}</div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table align="center"
                        style="width:400px !important; padding:20px 0 0 0 !important; margin:0 auto !important;"
                        id="table_kcal" border="0">
                        <tr>
                            <td colspan="2" class="titulos_rosas"
                                style="color:#EE8099;width:225px; text-align:center; font-size:18px;">ESTE ALIMENTO
                                EQUIVALE A: </td>
                        </tr>
                        <tr>
                            <td style="width:64px; height:64px; text-align:right;">
                                <img style="width:64px;height:64px;"
                                    src="https://dietasi.com/rapidosyligeros/public/mail_images/iconoPostre1.png">
                            </td>
                            <td
                                style="color:#9f2d6d;font-family:'headoh';font-size:48px; text-align: left;width: 135px;">
                                {{ $food->kcal }} KCAL
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" style=" color:#323232; font-size:14px;">
                                {{ $food->equivalent }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            @if ($infomail->comentario !== '')
                <tr>
                    <td align="center">
                        <table
                            style="padding:10px 0 0 0 !important;margin:40px auto !important; width:430px !important;"
                            id="table_nut_dice" border="0">
                            <tr>
                                <td rowspan="3" style="text-align:center;width:100px;" class="nut_dice_img"><img
                                        style=" width:100px; height:100px;"
                                        src="https://dietasi.com/img/nt/{{ $usuario->id }}.png"></td>
                                <td style="color:#ef5b7f; font-size:18px;text-align:left;width:225px;"
                                    class="titulos_rosas">Tu nutrióloga dice:</td>
                            </tr>
                            <tr>
                                <td style="color:#39b5ac;text-align:left; font-size:18px;" class="precios">Nut.
                                    {{ $usuario->nombre }}</td>
                            </tr>
                            <tr>
                                <td style="color:#323232; font-size:14px; text-align:left;" class="txt_nut_dice">
                                    {{ $infomail->comentario }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>

            @endif
            <tr>
                <td>
                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center"
                        style="color:#848486;font-size:12px;">
                        <tr>
                            <td align="center" style="line-height:16px;"><br>&copy; 2013-2014 NutricionSAS Todos los
                                derechos reservados. <a href="http://www.nutricionsas.com/politicas.html"
                                    style="width:auto;height:auto; display:inline;line-height:16px;">Pol&iacute;ticas de
                                    Privacidad</a><br />Euler 5 Col. Polanco M&eacute;xico, D.F. C.P 11560 Tel.
                                52507157<br />datospersonales@nutricionsas.com<br /><br /></td>
                        </tr>
                        <tr>
                            <td align="center" style="color:#000;line-height:16px;">Este mensaje fue enviado a
                                {{ $infomail->emailTo }}<br /><br /></td>
                        </tr>
                        <tr>
                            <td align="center" style="line-height:16px;">En NutricionSAS cuidamos tus datos personales.
                                Si no es usted el titular de este correo le agradecemos que haga caso omiso de este
                                mail.</td>
                        </tr>
                        <tr>
                            <td align="center"><br><img
                                    src="http://dietasporinternet.com/mailing/mailing29092014/logo_gris_nutricion.png"
                                    width="150" height="52"><br><br></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    @endforeach

</body>

</html>
