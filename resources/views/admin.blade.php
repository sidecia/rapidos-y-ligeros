@extends('layouts.app')
@section('content')
@section('title', 'Lo + Nuevo')

@section('content')
    <alimento-nuevos :user="{{ $userAuth }}" :alimentos='{{ $alimentos }}' :lastfoodspend='{{ $lastFoodsPend }}' :foodincomplete='{{ $foodincomplete }}'>
    </alimento-nuevos>
@endsection
