<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title','Rapidos y Ligeros')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  @php 
    $buscar = (isset($_GET['q']) and $_GET['q']<>'') ? $_GET['q'] : "" ;
  @endphp
    <div id="app" class="container-fluid">
      <v-app>
        <v-main>
            @auth 
              @include('partials.nav')  
            @endauth
            @auth 
              @include('partials.buscador',['buscarkey' => $buscar ]) 
            @endauth
            @yield('content')
        </v-main>
      </v-app>      
    </div>
</body>
</html>
