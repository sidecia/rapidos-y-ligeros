<navbar 
    :user       =   "{{ $userAuth }}" 
    :config        =   "{{ json_encode($config) }}" 
    :modulos        =   "{{ json_encode($modulos) }}" 
>
</navbar>
