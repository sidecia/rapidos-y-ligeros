-- MySQL dump 10.13  Distrib 5.7.25, for osx10.13 (x86_64)
--
-- Host: localhost    Database: rapidos_y_ligeros
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rl_carbohydrates`
--

DROP TABLE IF EXISTS `rl_carbohydrates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_carbohydrates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `total_quantity` int(11) NOT NULL,
  `simple` int(11) DEFAULT NULL,
  `complex` int(11) DEFAULT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_carbohydrates_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_carbohydrates_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_carbohydrates`
--

LOCK TABLES `rl_carbohydrates` WRITE;
/*!40000 ALTER TABLE `rl_carbohydrates` DISABLE KEYS */;
INSERT INTO `rl_carbohydrates` VALUES (1,40,0,40,1,NULL,NULL);
/*!40000 ALTER TABLE `rl_carbohydrates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_comments`
--

DROP TABLE IF EXISTS `rl_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `food_id` bigint(20) unsigned NOT NULL,
  `id_nut` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_comments_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_comments_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_comments`
--

LOCK TABLES `rl_comments` WRITE;
/*!40000 ALTER TABLE `rl_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_failed_jobs`
--

DROP TABLE IF EXISTS `rl_failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_failed_jobs`
--

LOCK TABLES `rl_failed_jobs` WRITE;
/*!40000 ALTER TABLE `rl_failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_fats`
--

DROP TABLE IF EXISTS `rl_fats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_fats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `total_quantity` int(11) NOT NULL,
  `saturated_fats` int(11) DEFAULT NULL,
  `unsaturated_fats` int(11) DEFAULT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_fats_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_fats_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_fats`
--

LOCK TABLES `rl_fats` WRITE;
/*!40000 ALTER TABLE `rl_fats` DISABLE KEYS */;
INSERT INTO `rl_fats` VALUES (1,25,20,80,1,NULL,NULL);
/*!40000 ALTER TABLE `rl_fats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_favorites`
--

DROP TABLE IF EXISTS `rl_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_favorites` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `food_id` bigint(20) unsigned NOT NULL,
  `id_nut` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_favorites_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_favorites_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_favorites`
--

LOCK TABLES `rl_favorites` WRITE;
/*!40000 ALTER TABLE `rl_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_categories`
--

DROP TABLE IF EXISTS `rl_food_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_categories`
--

LOCK TABLES `rl_food_categories` WRITE;
/*!40000 ALTER TABLE `rl_food_categories` DISABLE KEYS */;
INSERT INTO `rl_food_categories` VALUES (1,'Plato fuerte','plato fuerte','',NULL,NULL),(2,'Acompañamiento','Acompañamiento','',NULL,NULL),(3,'Aderezo','Aderezo','',NULL,NULL),(4,'Ensalada','Ensalada','',NULL,NULL),(5,'Fruta y preparados','Fruta y preparados','',NULL,NULL),(6,'Semillas&Topp','Semillas&Topp','',NULL,NULL),(7,'BebidaIMat','BebidaIMat','',NULL,NULL),(8,'Bebidas2','Bebidas2','',NULL,NULL),(9,'AntojoVP','AntojoVP','',NULL,NULL),(10,'Licuados','Licuados','',NULL,NULL),(11,'Smoothies','Smoothies','',NULL,NULL),(12,'Infusiones','Infusiones','',NULL,NULL),(13,'Bowls','Bowls','',NULL,NULL);
/*!40000 ALTER TABLE `rl_food_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_category_food`
--

DROP TABLE IF EXISTS `rl_food_category_food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_category_food` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `food_category_id` bigint(20) unsigned NOT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_food_category_food_food_category_id_foreign` (`food_category_id`),
  KEY `rl_food_category_food_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_food_category_food_food_category_id_foreign` FOREIGN KEY (`food_category_id`) REFERENCES `rl_food_categories` (`id`),
  CONSTRAINT `rl_food_category_food_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_category_food`
--

LOCK TABLES `rl_food_category_food` WRITE;
/*!40000 ALTER TABLE `rl_food_category_food` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_food_category_food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_category_time`
--

DROP TABLE IF EXISTS `rl_food_category_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_category_time` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `time_id` bigint(20) unsigned NOT NULL,
  `food_category_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_food_category_time_time_id_foreign` (`time_id`),
  KEY `rl_food_category_time_food_category_id_foreign` (`food_category_id`),
  CONSTRAINT `rl_food_category_time_food_category_id_foreign` FOREIGN KEY (`food_category_id`) REFERENCES `rl_food_categories` (`id`),
  CONSTRAINT `rl_food_category_time_time_id_foreign` FOREIGN KEY (`time_id`) REFERENCES `rl_times` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_category_time`
--

LOCK TABLES `rl_food_category_time` WRITE;
/*!40000 ALTER TABLE `rl_food_category_time` DISABLE KEYS */;
INSERT INTO `rl_food_category_time` VALUES (2,2,2,NULL,NULL),(3,3,4,NULL,NULL),(4,2,3,NULL,NULL),(5,2,1,NULL,NULL),(6,5,1,NULL,NULL),(7,5,2,NULL,NULL),(8,5,3,NULL,NULL),(9,4,1,NULL,NULL),(10,5,4,NULL,NULL),(11,4,2,NULL,NULL),(12,4,3,NULL,NULL);
/*!40000 ALTER TABLE `rl_food_category_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_galleries`
--

DROP TABLE IF EXISTS `rl_food_galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_galleries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_food_galleries_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_food_galleries_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_galleries`
--

LOCK TABLES `rl_food_galleries` WRITE;
/*!40000 ALTER TABLE `rl_food_galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_food_galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_ingredient`
--

DROP TABLE IF EXISTS `rl_food_ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_ingredient` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `ingredient_id` bigint(20) unsigned NOT NULL,
  `ingredient_measure_id` bigint(20) unsigned NOT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_food_ingredient_ingredient_id_foreign` (`ingredient_id`),
  KEY `rl_food_ingredient_ingredient_measure_id_foreign` (`ingredient_measure_id`),
  KEY `rl_food_ingredient_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_food_ingredient_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`),
  CONSTRAINT `rl_food_ingredient_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `rl_ingredients` (`id`),
  CONSTRAINT `rl_food_ingredient_ingredient_measure_id_foreign` FOREIGN KEY (`ingredient_measure_id`) REFERENCES `rl_ingredient_measures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_ingredient`
--

LOCK TABLES `rl_food_ingredient` WRITE;
/*!40000 ALTER TABLE `rl_food_ingredient` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_food_ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_pdfs`
--

DROP TABLE IF EXISTS `rl_food_pdfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_pdfs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url_pdf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_food_pdfs_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_food_pdfs_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_pdfs`
--

LOCK TABLES `rl_food_pdfs` WRITE;
/*!40000 ALTER TABLE `rl_food_pdfs` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_food_pdfs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_supermarket`
--

DROP TABLE IF EXISTS `rl_food_supermarket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_supermarket` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `food_id` bigint(20) unsigned NOT NULL,
  `supermarket_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_food_supermarket_food_id_foreign` (`food_id`),
  KEY `rl_food_supermarket_supermarket_id_foreign` (`supermarket_id`),
  CONSTRAINT `rl_food_supermarket_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`),
  CONSTRAINT `rl_food_supermarket_supermarket_id_foreign` FOREIGN KEY (`supermarket_id`) REFERENCES `rl_supermarkets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_supermarket`
--

LOCK TABLES `rl_food_supermarket` WRITE;
/*!40000 ALTER TABLE `rl_food_supermarket` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_food_supermarket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_food_videos`
--

DROP TABLE IF EXISTS `rl_food_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_food_videos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url_video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_food_videos_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_food_videos_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_food_videos`
--

LOCK TABLES `rl_food_videos` WRITE;
/*!40000 ALTER TABLE `rl_food_videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_food_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_foods`
--

DROP TABLE IF EXISTS `rl_foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_foods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `box_price` double(10,2) DEFAULT NULL,
  `diet_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_number` int(11) DEFAULT NULL,
  `piece_package` int(11) DEFAULT NULL,
  `kcal` double(8,2) DEFAULT NULL,
  `portion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portion_nutricionsas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nutritional_table_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nutritional_table` text COLLATE utf8mb4_unicode_ci,
  `strategies` text COLLATE utf8mb4_unicode_ci,
  `rules` text COLLATE utf8mb4_unicode_ci,
  `exaggerate` text COLLATE utf8mb4_unicode_ci,
  `risks` text COLLATE utf8mb4_unicode_ci,
  `generals` text COLLATE utf8mb4_unicode_ci,
  `equivalent` text COLLATE utf8mb4_unicode_ci,
  `preparation` text COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `valid_fields` text COLLATE utf8mb4_unicode_ci,
  `validated` tinyint(1) DEFAULT NULL,
  `id_nut` int(11) NOT NULL,
  `restaurant_price` double(8,2) DEFAULT NULL,
  `preparation_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `difficulty` int(11) DEFAULT NULL,
  `portability` int(11) DEFAULT NULL,
  `nutritionist_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_presentation_id` bigint(20) unsigned NOT NULL,
  `measure_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_foods_product_presentation_id_foreign` (`product_presentation_id`),
  KEY `rl_foods_measure_id_foreign` (`measure_id`),
  CONSTRAINT `rl_foods_measure_id_foreign` FOREIGN KEY (`measure_id`) REFERENCES `rl_measures` (`id`),
  CONSTRAINT `rl_foods_product_presentation_id_foreign` FOREIGN KEY (`product_presentation_id`) REFERENCES `rl_product_presentations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_foods`
--

LOCK TABLES `rl_foods` WRITE;
/*!40000 ALTER TABLE `rl_foods` DISABLE KEYS */;
INSERT INTO `rl_foods` VALUES (1,'Tapa De Huevo A La Albahaca',8.00,0.00,'Tapa De Huevo a la Albahaca 1 Pieza con 1 Piezas',1,4,220.00,'1','230','img_alimentos/105/croppedImg_1001836446.jpeg','info_tables/105/croppedImg_632727229.png','nut table','Si elijo poner este platillo por la mañana en automatico estare dando a mi paciente una fuente de energìa con el equilibrio ideal. Haciendo que comience su día motivado (por la preparación) y con energía por dicho contenido.\r\nLa preparación de el huevo de este platillo es con el huevo hervido con el proposito de brindar al paciente los mejores beneficios que el huevo pudiera aportar:\r\nProteinas de alta calidad ya que cuenta con todos los aminoácidos esenciales. \r\nEl huevo es uno de los alimentos que tiene más diversidad de vitaminas (A, E, D, B1, B2, B6 y B12). Mejorar nuestro sistema inmmunológico y aumentar la ingesta de antioxidantes. Además, de los minerales como el hierro, selenio, yodo, ácido fólico, zinc, entre otros.\r\n\r\nHaciendo de este platillo el mejor comienzo del dìa \r\n','-Importante tener actividad fisíca despues de su consumo\r\n-No alterar las cantidades\r\n-No utilizar el aceite para cocinar el huevo\r\n-No incluir la preparación en todos los dias (en los 3 menús) o días seguidos (ej. lunes,martes, miercoles)\r\n-Preferir este platillo por la mañana por su contenido de hidratos de carbono.','+ de 1 pieza ','TAPA DE HUEVO DURO ALBACA\r\nIngredientes:\r\nPan integral 1 pieza\r\nHuevo 1 pieza							\r\nJitomate 1pieza \r\nAceite de cártamo 1 cdta\r\nQueso parmesano 1 cdta \r\nAlbaca al gusto \r\n','\r\n\r\n¿DONDE COMPRAR?\r\nJITOMATES DESHIDRATADOS, \r\nPuedes adquirirlos en tienda\r\n1.\r\n2.\r\n3.\r\nCosto aproximado: $89\r\nSe piden como: Tomate deshidratado o Tomate Tostados al Sol\r\n(Agregar Presentación Imagen)\r\n\r\n','Pan con queso panela (rebanada 30g).Pan con pechuga de pavo 2 rebanadas delgadas. Pan con claras de huevo 2 piezas. ','Preparación:\r\n\r\n1.	Pon los huevos en una cazuela y cúbrelos con agua. Añade una pizca de sal para que luego se pelen más fácilmente. Pon la cazuela en el fuego y desde que el agua comience a hervir, deja cocer el huevo durante 10 minutos pasado ese tiempo, saca los huevos de la cazuela y déjalos enfriar.\r\n2.	Tostar pan (puedes usar tostador o a fuego lento).\r\n3.	Agrega a una sartén el jitomate finamente picado con el aceite y déjalo cocinar a fuego lento hasta tomar un color oscuro. Y unta en el pan. \r\n4.	Quita la cascara al huevo y córtalo a tu gusto, colocándolos sobre el pan \r\n5.	Espolvorea queso parmesano \r\n6.	Coloca albahaca al gusto ','pan integral,tapa,huevo,hervir,salado,tomate,gourmet','macro_grasa,macro_carbo,macro_percents,macro,preparacion,equivalente,generales,riesgos,exagerar,reglas,estrategias,porcion_nutricionsas,porcion,kcal,num_paq,id_cat,nombre_alimento,imagen_tabla_nutri,imagen_alimento,',1,336,35.00,'20 min',1,5,'L.N. Tania Yetzzi Cornejo Prado',4,2,NULL,NULL);
/*!40000 ALTER TABLE `rl_foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_ingredient_measures`
--

DROP TABLE IF EXISTS `rl_ingredient_measures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_ingredient_measures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `measure` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_ingredient_measures`
--

LOCK TABLES `rl_ingredient_measures` WRITE;
/*!40000 ALTER TABLE `rl_ingredient_measures` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_ingredient_measures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_ingredients`
--

DROP TABLE IF EXISTS `rl_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_ingredients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_ingredients`
--

LOCK TABLES `rl_ingredients` WRITE;
/*!40000 ALTER TABLE `rl_ingredients` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_measures`
--

DROP TABLE IF EXISTS `rl_measures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_measures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_measures`
--

LOCK TABLES `rl_measures` WRITE;
/*!40000 ALTER TABLE `rl_measures` DISABLE KEYS */;
INSERT INTO `rl_measures` VALUES (1,'Gramos',NULL,NULL),(2,'Piezas',NULL,NULL),(3,'Mililitros',NULL,NULL);
/*!40000 ALTER TABLE `rl_measures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_migrations`
--

DROP TABLE IF EXISTS `rl_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_migrations`
--

LOCK TABLES `rl_migrations` WRITE;
/*!40000 ALTER TABLE `rl_migrations` DISABLE KEYS */;
INSERT INTO `rl_migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2016_06_01_000001_create_oauth_auth_codes_table',1),(3,'2016_06_01_000002_create_oauth_access_tokens_table',1),(4,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(5,'2016_06_01_000004_create_oauth_clients_table',1),(6,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(7,'2018_08_08_100000_create_telescope_entries_table',1),(8,'2019_08_19_000000_create_failed_jobs_table',1),(9,'2020_07_28_160147_create_product_presentations_table',1),(10,'2020_07_28_160348_create_measures_table',1),(11,'2020_07_28_163207_create_times_table',1),(12,'2020_07_28_163718_create_food_categories_table',1),(13,'2020_07_28_163808_create_ingredients_table',1),(14,'2020_07_28_163856_create_ingredient_measures_table',1),(15,'2020_07_28_163945_create_supermarkets_table',1),(16,'2020_07_28_164852_create_foods_table',1),(17,'2020_07_28_165018_create_food_galleries_table',1),(18,'2020_07_28_165035_create_food_videos_table',1),(19,'2020_07_28_165055_create_food_pdfs_table',1),(20,'2020_07_28_165847_create_carbohydrates_table',1),(21,'2020_07_28_165958_create_proteins_table',1),(22,'2020_07_29_170732_create_comments_table',1),(23,'2020_07_29_171424_create_favorites_table',1),(24,'2020_07_29_171726_create_trendings_table',1),(25,'2020_07_29_172446_create_fats_table',1),(26,'2020_08_04_180457_create_food_category_time_table',1),(27,'2020_08_04_180532_create_food_category_food_table',1),(28,'2020_08_04_180726_create_food_ingredient_table',1),(29,'2020_08_04_180809_create_food_supermarket_table',1);
/*!40000 ALTER TABLE `rl_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_oauth_access_tokens`
--

DROP TABLE IF EXISTS `rl_oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_oauth_access_tokens`
--

LOCK TABLES `rl_oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `rl_oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `rl_oauth_access_tokens` VALUES ('0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718',NULL,1582930,NULL,'[]',0,'2020-08-21 06:07:45','2020-08-21 06:07:45','2021-08-21 01:07:45');
/*!40000 ALTER TABLE `rl_oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_oauth_auth_codes`
--

DROP TABLE IF EXISTS `rl_oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_oauth_auth_codes`
--

LOCK TABLES `rl_oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `rl_oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_oauth_clients`
--

DROP TABLE IF EXISTS `rl_oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1582931 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_oauth_clients`
--

LOCK TABLES `rl_oauth_clients` WRITE;
/*!40000 ALTER TABLE `rl_oauth_clients` DISABLE KEYS */;
INSERT INTO `rl_oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','MM5Crc5DEeYSfsebMX4DM7eHUmr2tfQJ13AQr1Jq','','http://localhost',1,0,0,'2020-08-18 00:52:14','2020-08-18 00:52:14'),(2,NULL,'Laravel Password Grant Client','7UuGUXcBia14rPov8vUXwpT6JFa3LIaJA1g9x56L','users','http://localhost',0,1,0,'2020-08-18 00:52:14','2020-08-18 00:52:14'),(1582930,NULL,'Rapidos y ligeros Access','Y61Jx7tc5i1LgbXbbOR1opYy6LVi3ALSk3uUcWsV','','http://localhost',0,0,0,NULL,NULL);
/*!40000 ALTER TABLE `rl_oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `rl_oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_oauth_personal_access_clients`
--

LOCK TABLES `rl_oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `rl_oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `rl_oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_oauth_refresh_tokens`
--

LOCK TABLES `rl_oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `rl_oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_product_presentations`
--

DROP TABLE IF EXISTS `rl_product_presentations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_product_presentations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_product_presentations`
--

LOCK TABLES `rl_product_presentations` WRITE;
/*!40000 ALTER TABLE `rl_product_presentations` DISABLE KEYS */;
INSERT INTO `rl_product_presentations` VALUES (1,'Botella',NULL,NULL),(2,'Bolsa',NULL,NULL),(3,'Paquete',NULL,NULL),(4,'Pieza',NULL,NULL),(5,'Envase',NULL,NULL),(6,'Cucharada',NULL,NULL),(7,'Cucharadita',NULL,NULL),(8,'Del tamaño de tu mano',NULL,NULL),(9,'Porción Restaurante',NULL,NULL),(10,'Taza',NULL,NULL);
/*!40000 ALTER TABLE `rl_product_presentations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_proteins`
--

DROP TABLE IF EXISTS `rl_proteins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_proteins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `total_quantity` int(11) NOT NULL,
  `food_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_proteins_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_proteins_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_proteins`
--

LOCK TABLES `rl_proteins` WRITE;
/*!40000 ALTER TABLE `rl_proteins` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_proteins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_supermarkets`
--

DROP TABLE IF EXISTS `rl_supermarkets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_supermarkets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_supermarkets`
--

LOCK TABLES `rl_supermarkets` WRITE;
/*!40000 ALTER TABLE `rl_supermarkets` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_supermarkets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_telescope_entries`
--

DROP TABLE IF EXISTS `rl_telescope_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_telescope_entries` (
  `sequence` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sequence`),
  UNIQUE KEY `rl_telescope_entries_uuid_unique` (`uuid`),
  KEY `rl_telescope_entries_batch_id_index` (`batch_id`),
  KEY `rl_telescope_entries_family_hash_index` (`family_hash`),
  KEY `rl_telescope_entries_created_at_index` (`created_at`),
  KEY `rl_telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_telescope_entries`
--

LOCK TABLES `rl_telescope_entries` WRITE;
/*!40000 ALTER TABLE `rl_telescope_entries` DISABLE KEYS */;
INSERT INTO `rl_telescope_entries` VALUES (1,'9155afb5-1a03-4d07-bf17-a379b28c0ad3','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'cache','{\"type\":\"missed\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(2,'9155afb5-1e00-4c10-924e-5f1bbc2836fb','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'cache','{\"type\":\"missed\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(3,'9155afb5-1ea9-402a-b99c-46ace652bffb','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'cache','{\"type\":\"set\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"value\":1597972121,\"expiration\":60,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(4,'9155afb5-1f12-4d58-a850-59e5cd0076da','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'cache','{\"type\":\"missed\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(5,'9155afb5-1f7c-443d-985d-f5bee04ae4d9','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'cache','{\"type\":\"set\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":0,\"expiration\":60,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(6,'9155afb5-2c73-4131-9f2a-d6d3801dd17e','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'601e74ffda03f0936e4db21a34b2a8ee43da7e3569f941a5435d6de95c6005df48bba6f7c0578177\' limit 1\",\"time\":\"0.61\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(7,'9155afb5-3097-434f-a0b7-ec05fc8aef4b','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":1,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(8,'9155afb5-316f-4e0c-bf97-d71e1d641998','9155afb5-323f-41d7-9012-0bb739d36817',NULL,1,'request','{\"ip_address\":\"127.0.0.1\",\"uri\":\"\\/api\\/times\",\"method\":\"GET\",\"controller_action\":\"App\\\\Http\\\\Controllers\\\\Api\\\\TimeController@index\",\"middleware\":[\"api\",\"authorizedClient\"],\"headers\":{\"accept\":\"application\\/json\",\"authorization\":\"********\",\"user-agent\":\"PostmanRuntime\\/7.26.3\",\"cache-control\":\"no-cache\",\"postman-token\":\"64c8a551-fd48-4bd0-90c5-c512ebcf3906\",\"host\":\"localhost:8000\",\"accept-encoding\":\"gzip, deflate, br\",\"connection\":\"keep-alive\"},\"payload\":[],\"session\":[],\"response_status\":401,\"response\":{\"message\":\"Unauthenticated.\"},\"duration\":325,\"memory\":22,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:41'),(9,'9155afbb-0f3c-4d12-96f6-fdd1a563ef4b','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":1,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(10,'9155afbb-1118-4ef7-94fd-5b05b69947c3','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"value\":1597972121,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(11,'9155afbb-116c-4efe-8c30-b80978c4007c','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":1,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(12,'9155afbb-17c2-4288-bc3b-9cfaa221a95d','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_clients` where `id` = \'1582930\' limit 1\",\"time\":\"0.65\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"a3cbba0dbcf96068079432bb3446906b\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(13,'9155afbb-19e3-4969-8f88-916aeebc9420','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_clients` where `id` = \'1582930\' limit 1\",\"time\":\"0.57\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"a3cbba0dbcf96068079432bb3446906b\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(14,'9155afbb-1c24-4f52-bc4f-dc011d50ba9b','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_clients` where `id` = \'1582930\' limit 1\",\"time\":\"1.01\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"a3cbba0dbcf96068079432bb3446906b\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(15,'9155afbb-3525-4902-b580-cc07de791aad','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `rl_oauth_access_tokens` (`id`, `user_id`, `client_id`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) values (\'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\', null, \'1582930\', \'[]\', 0, \'2020-08-21 01:07:45\', \'2020-08-21 01:07:45\', \'2021-08-21 01:07:45\')\",\"time\":\"41.76\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"787643052bc0ba7111b4ddfeb0a4161a\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(16,'9155afbb-3641-47e4-8547-87984e429fe8','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'model','{\"action\":\"created\",\"model\":\"Laravel\\\\Passport\\\\Token:0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(17,'9155afbb-3840-4304-ac23-9b18530352aa','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'event','{\"name\":\"Laravel\\\\Passport\\\\Events\\\\AccessTokenCreated\",\"payload\":{\"tokenId\":\"0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\",\"userId\":null,\"clientId\":\"1582930\"},\"listeners\":[],\"broadcast\":false,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(18,'9155afbb-4030-4a42-911b-147a871ff0c6','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":2,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(19,'9155afbb-40d0-4cd8-a6d9-a6c5d7446cd8','9155afbb-41e5-4820-9489-a642b173a7a4',NULL,1,'request','{\"ip_address\":\"127.0.0.1\",\"uri\":\"\\/oauth\\/token\",\"method\":\"POST\",\"controller_action\":\"\\\\Laravel\\\\Passport\\\\Http\\\\Controllers\\\\AccessTokenController@issueToken\",\"middleware\":[\"throttle\"],\"headers\":{\"accept\":\"application\\/json\",\"user-agent\":\"PostmanRuntime\\/7.26.3\",\"cache-control\":\"no-cache\",\"postman-token\":\"b2edc8c5-12fd-4989-8c2d-fd24d02612ee\",\"host\":\"localhost:8000\",\"accept-encoding\":\"gzip, deflate, br\",\"connection\":\"keep-alive\",\"content-type\":\"multipart\\/form-data; boundary=--------------------------601674082055229225325740\",\"content-length\":\"450\"},\"payload\":{\"client_id\":\"1582930\",\"client_secret\":\"Y61Jx7tc5i1LgbXbbOR1opYy6LVi3ALSk3uUcWsV\",\"grant_type\":\"client_credentials\"},\"session\":[],\"response_status\":200,\"response\":{\"token_type\":\"Bearer\",\"expires_in\":31536000,\"access_token\":\"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxNTgyOTMwIiwianRpIjoiMGNlNmJjMWZlNmU0NGVjYjNlNzFmMTdlOWFkMzgwMzIzMGY1MWE1OGIzMGIwMTU3N2ZhNTBhZGI5N2I3OTI2M2ZhYTU3Y2JjODdjZTA3MTgiLCJpYXQiOjE1OTc5NzIwNjUsIm5iZiI6MTU5Nzk3MjA2NSwiZXhwIjoxNjI5NTA4MDY1LCJzdWIiOiIiLCJzY29wZXMiOltdfQ.swbC_OPTmjHK-jHDd-HfJjp-w5W8ucqG3oc3KUKMJ2PPc_PXOerJE3fQlZ-aAJnFvTN-NFIkZI2LurXct-pybU9fgSNtAFlmO329zR0utP6rT48bhc7YO8gR-BWt4jIapoPyOrbd-WA8-RCrES9VZSIMqiHpQd7GjJr8dA5TUJizIj7gY325P6qTWvRviXhY7BkfHAVpSTyl5M_-CVVVU8jAlSdST0GZoMQKJ9Y5BdV7nqJX_WR029qU9zTpeBzCVIGN9OXV2Is3ccBsZcNtl5bxV8pb6U6Sh-kj_ZD0VI3ZLOxr1aVoX-tWlnzQ8qKGtKKm0tzFZkySnLaziw3xM_S5VhDwESrBstGG4upSFeuyamveDqPQMUZVfyaZE5wcdIaPzM3dZ08RPsowvPggOkjdV64C20aWbvBJfYChQirdw1OY8Q4l4Wh7BwJAHD7P5UVaseAduV9FI2N8IZxJE-1ITqrcIO6EKlLoDsYxgQjcja5GecpBFiS7UR2XarOK4MZv4TzFCWnuKZajHTVDQzA-Sul-_tAtrO3XmASDaTsr4jviI-ng4xgCY3puoEuKXGmJbvK27M7LdrTtKh3RjSddzn8wUTRKp2qh3A0rsDVOk0muTKhToW64GptD9ewXiZ5BUK2eBV4VXC4yGdtSjn0MlRYUJo7lheoye_vm-Hg\"},\"duration\":377,\"memory\":12,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:45'),(20,'9155afcb-14da-4737-ac36-c1d901592c34','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":2,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(21,'9155afcb-16b1-40a1-9703-d51c80cc902e','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"value\":1597972121,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(22,'9155afcb-1704-4559-92cd-f975fd875517','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":2,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(23,'9155afcb-2fab-4c26-987a-d9f6844b9e38','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"42.37\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(24,'9155afcb-31e1-4082-9a71-6331816bf314','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.66\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(25,'9155afcb-4398-4668-b911-b28395a3db3f','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_times`\",\"time\":\"40.90\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/TimeController.php\",\"line\":18,\"hash\":\"0db326a5b0d015dea556165c964c676f\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(26,'9155afcb-5937-4024-9961-71774f4470c5','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select `rl_food_categories`.*, `rl_food_category_time`.`time_id` as `pivot_time_id`, `rl_food_category_time`.`food_category_id` as `pivot_food_category_id` from `rl_food_categories` inner join `rl_food_category_time` on `rl_food_categories`.`id` = `rl_food_category_time`.`food_category_id` where `rl_food_category_time`.`time_id` in (2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)\",\"time\":\"41.61\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/TimeController.php\",\"line\":18,\"hash\":\"97cc2f8ae796f2298d9a55c42ad81f75\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(27,'9155afcb-5f50-4c46-95e4-5f35357003a2','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":3,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(28,'9155afcb-5fdd-45ee-9748-6a784ded38b3','9155afcb-60a3-4b7c-914e-6cdf7eeb53b5',NULL,1,'request','{\"ip_address\":\"127.0.0.1\",\"uri\":\"\\/api\\/times\",\"method\":\"GET\",\"controller_action\":\"App\\\\Http\\\\Controllers\\\\Api\\\\TimeController@index\",\"middleware\":[\"api\",\"authorizedClient\"],\"headers\":{\"accept\":\"application\\/json\",\"authorization\":\"********\",\"user-agent\":\"PostmanRuntime\\/7.26.3\",\"cache-control\":\"no-cache\",\"postman-token\":\"a86d27af-bf4d-4031-a8d4-059e8c2cdeb8\",\"host\":\"localhost:8000\",\"accept-encoding\":\"gzip, deflate, br\",\"connection\":\"keep-alive\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"success\":true,\"benchmark\":{\"memory\":\"20.1MB\",\"time\":0.41671180725097656},\"data\":[{\"id\":2,\"name\":\"desayuno\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[{\"id\":2,\"name\":\"Acompa\\u00f1amiento\",\"description\":\"Acompa\\u00f1amiento\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":2,\"food_category_id\":2}},{\"id\":3,\"name\":\"Aderezo\",\"description\":\"Aderezo\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":2,\"food_category_id\":3}},{\"id\":1,\"name\":\"Plato fuerte\",\"description\":\"plato fuerte\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":2,\"food_category_id\":1}}]},{\"id\":3,\"name\":\"shot de energ\\u00eda\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[{\"id\":4,\"name\":\"Ensalada\",\"description\":\"Ensalada\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":3,\"food_category_id\":4}}]},{\"id\":4,\"name\":\"media ma\\u00f1ana\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[{\"id\":1,\"name\":\"Plato fuerte\",\"description\":\"plato fuerte\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":4,\"food_category_id\":1}},{\"id\":2,\"name\":\"Acompa\\u00f1amiento\",\"description\":\"Acompa\\u00f1amiento\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":4,\"food_category_id\":2}},{\"id\":3,\"name\":\"Aderezo\",\"description\":\"Aderezo\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":4,\"food_category_id\":3}}]},{\"id\":5,\"name\":\"saciador comida\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[{\"id\":1,\"name\":\"Plato fuerte\",\"description\":\"plato fuerte\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":5,\"food_category_id\":1}},{\"id\":2,\"name\":\"Acompa\\u00f1amiento\",\"description\":\"Acompa\\u00f1amiento\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":5,\"food_category_id\":2}},{\"id\":3,\"name\":\"Aderezo\",\"description\":\"Aderezo\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":5,\"food_category_id\":3}},{\"id\":4,\"name\":\"Ensalada\",\"description\":\"Ensalada\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"pivot\":{\"time_id\":5,\"food_category_id\":4}}]},{\"id\":6,\"name\":\"comida\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]},{\"id\":7,\"name\":\"media tarde\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]},{\"id\":8,\"name\":\"saciador cena\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]},{\"id\":9,\"name\":\"antes del gym\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]},{\"id\":10,\"name\":\"durante el gym\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]},{\"id\":11,\"name\":\"despu\\u00e9s del gym\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]},{\"id\":12,\"name\":\"durante esfuerzo intelectual \",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]},{\"id\":13,\"name\":\"cena\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"categories\":[]}]},\"duration\":423,\"memory\":8,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:07:55'),(29,'9155afd6-5e52-46c5-9fee-376261330e3e','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":3,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(30,'9155afd6-608a-49a4-b874-9f65930ab46e','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"value\":1597972121,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(31,'9155afd6-60e0-465c-99f7-b1ad42c6f229','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":3,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(32,'9155afd6-673c-4e80-81ea-876e284f1ce0','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.65\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(33,'9155afd6-68c4-4270-9cfc-4bcfcb4d057f','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.51\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(34,'9155afd6-6a0d-43f9-8a89-9eefdfc1f9bb','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_food_categories`\",\"time\":\"0.45\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodCategoryController.php\",\"line\":18,\"hash\":\"44335c0304603baed2c24e25e5c99cff\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(35,'9155afd6-6e10-4a02-a54c-e20898247feb','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select `rl_times`.*, `rl_food_category_time`.`food_category_id` as `pivot_food_category_id`, `rl_food_category_time`.`time_id` as `pivot_time_id` from `rl_times` inner join `rl_food_category_time` on `rl_times`.`id` = `rl_food_category_time`.`time_id` where `rl_food_category_time`.`food_category_id` in (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)\",\"time\":\"0.82\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodCategoryController.php\",\"line\":18,\"hash\":\"ec855597b7a080321d0d94bfe42d716f\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(36,'9155afd6-7325-48a8-aea5-3b4bb5d89ff1','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":4,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(37,'9155afd6-740d-4f0a-9a85-08bb1515f87d','9155afd6-7507-4dd9-a93b-cbb0fae4a2bd',NULL,1,'request','{\"ip_address\":\"127.0.0.1\",\"uri\":\"\\/api\\/food-categories\",\"method\":\"GET\",\"controller_action\":\"App\\\\Http\\\\Controllers\\\\Api\\\\FoodCategoryController@index\",\"middleware\":[\"api\",\"authorizedClient\"],\"headers\":{\"accept\":\"application\\/json\",\"authorization\":\"********\",\"user-agent\":\"PostmanRuntime\\/7.26.3\",\"cache-control\":\"no-cache\",\"postman-token\":\"09e71ea9-9124-4a7c-a436-50c85ff1ca1a\",\"host\":\"localhost:8000\",\"accept-encoding\":\"gzip, deflate, br\",\"connection\":\"keep-alive\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"success\":true,\"benchmark\":{\"memory\":\"20.11MB\",\"time\":0.2822530269622803},\"data\":[{\"id\":1,\"name\":\"Plato fuerte\",\"description\":\"plato fuerte\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[{\"id\":2,\"name\":\"desayuno\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":1,\"time_id\":2}},{\"id\":5,\"name\":\"saciador comida\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":1,\"time_id\":5}},{\"id\":4,\"name\":\"media ma\\u00f1ana\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":1,\"time_id\":4}}]},{\"id\":2,\"name\":\"Acompa\\u00f1amiento\",\"description\":\"Acompa\\u00f1amiento\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[{\"id\":2,\"name\":\"desayuno\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":2,\"time_id\":2}},{\"id\":5,\"name\":\"saciador comida\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":2,\"time_id\":5}},{\"id\":4,\"name\":\"media ma\\u00f1ana\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":2,\"time_id\":4}}]},{\"id\":3,\"name\":\"Aderezo\",\"description\":\"Aderezo\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[{\"id\":2,\"name\":\"desayuno\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":3,\"time_id\":2}},{\"id\":5,\"name\":\"saciador comida\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":3,\"time_id\":5}},{\"id\":4,\"name\":\"media ma\\u00f1ana\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":3,\"time_id\":4}}]},{\"id\":4,\"name\":\"Ensalada\",\"description\":\"Ensalada\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[{\"id\":3,\"name\":\"shot de energ\\u00eda\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":4,\"time_id\":3}},{\"id\":5,\"name\":\"saciador comida\",\"order\":1,\"created_at\":null,\"updated_at\":null,\"pivot\":{\"food_category_id\":4,\"time_id\":5}}]},{\"id\":5,\"name\":\"Fruta y preparados\",\"description\":\"Fruta y preparados\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":6,\"name\":\"Semillas&Topp\",\"description\":\"Semillas&Topp\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":7,\"name\":\"BebidaIMat\",\"description\":\"BebidaIMat\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":8,\"name\":\"Bebidas2\",\"description\":\"Bebidas2\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":9,\"name\":\"AntojoVP\",\"description\":\"AntojoVP\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":10,\"name\":\"Licuados\",\"description\":\"Licuados\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":11,\"name\":\"Smoothies\",\"description\":\"Smoothies\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":12,\"name\":\"Infusiones\",\"description\":\"Infusiones\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]},{\"id\":13,\"name\":\"Bowls\",\"description\":\"Bowls\",\"video\":\"\",\"created_at\":null,\"updated_at\":null,\"times\":[]}]},\"duration\":290,\"memory\":6,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:03'),(38,'9155afe5-6c74-480c-9e92-5a628f58d95d','9155afe5-6ffe-4885-9d31-9f1caef946e9','3d0acfb8281d14c4f49c11d57bf04f5f',0,'exception','{\"class\":\"Illuminate\\\\Database\\\\QueryException\",\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":671,\"message\":\"SQLSTATE[42S02]: Base table or view not found: 1146 Table \'rapidos_y_ligeros.rl_rl_foods\' doesn\'t exist (SQL: select * from `rl_rl_foods` where `id_nut` = 336)\",\"context\":null,\"trace\":[{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":631},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":339},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2205},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2193},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2688},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2194},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":539},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":523},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":75},[],{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Controller.php\",\"line\":54},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/ControllerDispatcher.php\",\"line\":45},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":239},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":196},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":685},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/passport\\/src\\/Http\\/Middleware\\/CheckCredentials.php\",\"line\":72},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/SubstituteBindings.php\",\"line\":41},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/ThrottleRequests.php\",\"line\":59},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":687},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":662},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":628},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":617},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":165},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/ValidatePostSize.php\",\"line\":27},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/CheckForMaintenanceMode.php\",\"line\":63},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fruitcake\\/laravel-cors\\/src\\/HandleCors.php\",\"line\":37},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fideloper\\/proxy\\/src\\/TrustProxies.php\",\"line\":57},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":140},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":109},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/server.php\",\"line\":21}],\"line_preview\":{\"662\":\"        \\/\\/ took to execute and log the query SQL, bindings and time in our memory.\",\"663\":\"        try {\",\"664\":\"            $result = $callback($query, $bindings);\",\"665\":\"        }\",\"666\":\"\",\"667\":\"        \\/\\/ If an exception occurs when attempting to run a query, we\'ll format the error\",\"668\":\"        \\/\\/ message to include the bindings with SQL, which will make this exception a\",\"669\":\"        \\/\\/ lot more helpful to the developer instead of just the database\'s errors.\",\"670\":\"        catch (Exception $e) {\",\"671\":\"            throw new QueryException(\",\"672\":\"                $query, $this->prepareBindings($bindings), $e\",\"673\":\"            );\",\"674\":\"        }\",\"675\":\"\",\"676\":\"        return $result;\",\"677\":\"    }\",\"678\":\"\",\"679\":\"    \\/**\",\"680\":\"     * Log a query in the connection\'s query log.\",\"681\":\"     *\"},\"hostname\":\"WKMUS9337096\",\"occurrences\":1}','2020-08-21 01:08:12'),(39,'9155afe5-5b89-41fb-8564-e335b3fe21fe','9155afe5-6ffe-4885-9d31-9f1caef946e9',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":4,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:12'),(40,'9155afe5-5d7a-4ad3-b1b2-3f024b0ea6d0','9155afe5-6ffe-4885-9d31-9f1caef946e9',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"value\":1597972121,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:12'),(41,'9155afe5-5dcb-4f0a-aab8-5fce576a19a7','9155afe5-6ffe-4885-9d31-9f1caef946e9',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":4,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:12'),(42,'9155afe5-6458-4646-9aae-bf5910affbaa','9155afe5-6ffe-4885-9d31-9f1caef946e9',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.62\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:12'),(43,'9155afe5-65d4-4623-9ae1-95cf754c1424','9155afe5-6ffe-4885-9d31-9f1caef946e9',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.54\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:12'),(44,'9155afe5-6e31-4c79-bb2e-5a74a87f2acf','9155afe5-6ffe-4885-9d31-9f1caef946e9',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":5,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:12'),(45,'9155afe5-6f00-4c6e-8df5-09cb5251bdf7','9155afe5-6ffe-4885-9d31-9f1caef946e9',NULL,1,'request','{\"ip_address\":\"127.0.0.1\",\"uri\":\"\\/api\\/foods\",\"method\":\"POST\",\"controller_action\":\"App\\\\Http\\\\Controllers\\\\Api\\\\FoodController@show\",\"middleware\":[\"api\",\"authorizedClient\"],\"headers\":{\"accept\":\"application\\/json\",\"authorization\":\"********\",\"content-type\":\"application\\/json\",\"user-agent\":\"PostmanRuntime\\/7.26.3\",\"cache-control\":\"no-cache\",\"postman-token\":\"790c9972-5292-4994-a7ae-70d7a67b74ab\",\"host\":\"localhost:8000\",\"accept-encoding\":\"gzip, deflate, br\",\"connection\":\"keep-alive\",\"content-length\":\"32\"},\"payload\":{\"id\":null,\"nut\":336},\"session\":[],\"response_status\":500,\"response\":{\"message\":\"SQLSTATE[42S02]: Base table or view not found: 1146 Table \'rapidos_y_ligeros.rl_rl_foods\' doesn\'t exist (SQL: select * from `rl_rl_foods` where `id_nut` = 336)\",\"exception\":\"Illuminate\\\\Database\\\\QueryException\",\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":671,\"trace\":[{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":631,\"function\":\"runQueryCallback\",\"class\":\"Illuminate\\\\Database\\\\Connection\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":339,\"function\":\"run\",\"class\":\"Illuminate\\\\Database\\\\Connection\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2205,\"function\":\"select\",\"class\":\"Illuminate\\\\Database\\\\Connection\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2193,\"function\":\"runSelect\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2688,\"function\":\"Illuminate\\\\Database\\\\Query\\\\{closure}\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2194,\"function\":\"onceWithColumns\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":539,\"function\":\"get\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":523,\"function\":\"getModels\",\"class\":\"Illuminate\\\\Database\\\\Eloquent\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":75,\"function\":\"get\",\"class\":\"Illuminate\\\\Database\\\\Eloquent\\\\Builder\",\"type\":\"->\"},{\"function\":\"show\",\"class\":\"App\\\\Http\\\\Controllers\\\\Api\\\\FoodController\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Controller.php\",\"line\":54,\"function\":\"call_user_func_array\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/ControllerDispatcher.php\",\"line\":45,\"function\":\"callAction\",\"class\":\"Illuminate\\\\Routing\\\\Controller\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":239,\"function\":\"dispatch\",\"class\":\"Illuminate\\\\Routing\\\\ControllerDispatcher\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":196,\"function\":\"runController\",\"class\":\"Illuminate\\\\Routing\\\\Route\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":685,\"function\":\"run\",\"class\":\"Illuminate\\\\Routing\\\\Route\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128,\"function\":\"Illuminate\\\\Routing\\\\{closure}\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/passport\\/src\\/Http\\/Middleware\\/CheckCredentials.php\",\"line\":72,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Laravel\\\\Passport\\\\Http\\\\Middleware\\\\CheckCredentials\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/SubstituteBindings.php\",\"line\":41,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Routing\\\\Middleware\\\\SubstituteBindings\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/ThrottleRequests.php\",\"line\":59,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Routing\\\\Middleware\\\\ThrottleRequests\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":687,\"function\":\"then\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":662,\"function\":\"runRouteWithinStack\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":628,\"function\":\"runRoute\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":617,\"function\":\"dispatchToRoute\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":165,\"function\":\"dispatch\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128,\"function\":\"Illuminate\\\\Foundation\\\\Http\\\\{closure}\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Kernel\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\TransformsRequest\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\TransformsRequest\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/ValidatePostSize.php\",\"line\":27,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\ValidatePostSize\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/CheckForMaintenanceMode.php\",\"line\":63,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\CheckForMaintenanceMode\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fruitcake\\/laravel-cors\\/src\\/HandleCors.php\",\"line\":37,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Fruitcake\\\\Cors\\\\HandleCors\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fideloper\\/proxy\\/src\\/TrustProxies.php\",\"line\":57,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Fideloper\\\\Proxy\\\\TrustProxies\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":140,\"function\":\"then\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":109,\"function\":\"sendRequestThroughRouter\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Kernel\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Kernel\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/server.php\",\"line\":21,\"function\":\"require_once\"}]},\"duration\":271,\"memory\":4,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:12'),(46,'9155afeb-bb77-401b-bf37-50cd7674f867','9155afeb-bf45-4c62-b069-ee67e4360156','3d0acfb8281d14c4f49c11d57bf04f5f',1,'exception','{\"class\":\"Illuminate\\\\Database\\\\QueryException\",\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":671,\"message\":\"SQLSTATE[42S02]: Base table or view not found: 1146 Table \'rapidos_y_ligeros.rl_rl_foods\' doesn\'t exist (SQL: select * from `rl_rl_foods`)\",\"context\":null,\"trace\":[{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":631},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":339},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2205},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2193},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2688},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2194},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":539},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":523},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27},[],{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Controller.php\",\"line\":54},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/ControllerDispatcher.php\",\"line\":45},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":239},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":196},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":685},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/passport\\/src\\/Http\\/Middleware\\/CheckCredentials.php\",\"line\":72},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/SubstituteBindings.php\",\"line\":41},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/ThrottleRequests.php\",\"line\":59},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":687},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":662},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":628},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":617},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":165},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/ValidatePostSize.php\",\"line\":27},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/CheckForMaintenanceMode.php\",\"line\":63},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fruitcake\\/laravel-cors\\/src\\/HandleCors.php\",\"line\":37},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fideloper\\/proxy\\/src\\/TrustProxies.php\",\"line\":57},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":140},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":109},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/server.php\",\"line\":21}],\"line_preview\":{\"662\":\"        \\/\\/ took to execute and log the query SQL, bindings and time in our memory.\",\"663\":\"        try {\",\"664\":\"            $result = $callback($query, $bindings);\",\"665\":\"        }\",\"666\":\"\",\"667\":\"        \\/\\/ If an exception occurs when attempting to run a query, we\'ll format the error\",\"668\":\"        \\/\\/ message to include the bindings with SQL, which will make this exception a\",\"669\":\"        \\/\\/ lot more helpful to the developer instead of just the database\'s errors.\",\"670\":\"        catch (Exception $e) {\",\"671\":\"            throw new QueryException(\",\"672\":\"                $query, $this->prepareBindings($bindings), $e\",\"673\":\"            );\",\"674\":\"        }\",\"675\":\"\",\"676\":\"        return $result;\",\"677\":\"    }\",\"678\":\"\",\"679\":\"    \\/**\",\"680\":\"     * Log a query in the connection\'s query log.\",\"681\":\"     *\"},\"hostname\":\"WKMUS9337096\",\"occurrences\":2}','2020-08-21 01:08:17'),(47,'9155afeb-ad81-44bd-b6d4-0d127204f597','9155afeb-bf45-4c62-b069-ee67e4360156',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":5,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:17'),(48,'9155afeb-af66-4b85-b139-90f512810f18','9155afeb-bf45-4c62-b069-ee67e4360156',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"value\":1597972121,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:17'),(49,'9155afeb-afd7-403e-8e00-ec24d79ea5c1','9155afeb-bf45-4c62-b069-ee67e4360156',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":5,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:17'),(50,'9155afeb-b60f-40ea-a0aa-d27932c106e1','9155afeb-bf45-4c62-b069-ee67e4360156',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.86\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:17'),(51,'9155afeb-b7ab-4a0f-930f-fd4671bc748a','9155afeb-bf45-4c62-b069-ee67e4360156',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.53\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:17'),(52,'9155afeb-bd51-490f-a3b6-82299bd8d461','9155afeb-bf45-4c62-b069-ee67e4360156',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":6,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:17'),(53,'9155afeb-be06-4869-b365-66043099bbde','9155afeb-bf45-4c62-b069-ee67e4360156',NULL,1,'request','{\"ip_address\":\"127.0.0.1\",\"uri\":\"\\/api\\/foods\",\"method\":\"GET\",\"controller_action\":\"App\\\\Http\\\\Controllers\\\\Api\\\\FoodController@index\",\"middleware\":[\"api\",\"authorizedClient\"],\"headers\":{\"accept\":\"application\\/json\",\"authorization\":\"********\",\"user-agent\":\"PostmanRuntime\\/7.26.3\",\"cache-control\":\"no-cache\",\"postman-token\":\"0d9c791a-2b6f-428a-806a-e126e453a66f\",\"host\":\"localhost:8000\",\"accept-encoding\":\"gzip, deflate, br\",\"connection\":\"keep-alive\"},\"payload\":[],\"session\":[],\"response_status\":500,\"response\":{\"message\":\"SQLSTATE[42S02]: Base table or view not found: 1146 Table \'rapidos_y_ligeros.rl_rl_foods\' doesn\'t exist (SQL: select * from `rl_rl_foods`)\",\"exception\":\"Illuminate\\\\Database\\\\QueryException\",\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":671,\"trace\":[{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":631,\"function\":\"runQueryCallback\",\"class\":\"Illuminate\\\\Database\\\\Connection\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Connection.php\",\"line\":339,\"function\":\"run\",\"class\":\"Illuminate\\\\Database\\\\Connection\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2205,\"function\":\"select\",\"class\":\"Illuminate\\\\Database\\\\Connection\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2193,\"function\":\"runSelect\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2688,\"function\":\"Illuminate\\\\Database\\\\Query\\\\{closure}\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Query\\/Builder.php\",\"line\":2194,\"function\":\"onceWithColumns\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":539,\"function\":\"get\",\"class\":\"Illuminate\\\\Database\\\\Query\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Database\\/Eloquent\\/Builder.php\",\"line\":523,\"function\":\"getModels\",\"class\":\"Illuminate\\\\Database\\\\Eloquent\\\\Builder\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"function\":\"get\",\"class\":\"Illuminate\\\\Database\\\\Eloquent\\\\Builder\",\"type\":\"->\"},{\"function\":\"index\",\"class\":\"App\\\\Http\\\\Controllers\\\\Api\\\\FoodController\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Controller.php\",\"line\":54,\"function\":\"call_user_func_array\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/ControllerDispatcher.php\",\"line\":45,\"function\":\"callAction\",\"class\":\"Illuminate\\\\Routing\\\\Controller\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":239,\"function\":\"dispatch\",\"class\":\"Illuminate\\\\Routing\\\\ControllerDispatcher\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Route.php\",\"line\":196,\"function\":\"runController\",\"class\":\"Illuminate\\\\Routing\\\\Route\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":685,\"function\":\"run\",\"class\":\"Illuminate\\\\Routing\\\\Route\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128,\"function\":\"Illuminate\\\\Routing\\\\{closure}\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/passport\\/src\\/Http\\/Middleware\\/CheckCredentials.php\",\"line\":72,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Laravel\\\\Passport\\\\Http\\\\Middleware\\\\CheckCredentials\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/SubstituteBindings.php\",\"line\":41,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Routing\\\\Middleware\\\\SubstituteBindings\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Middleware\\/ThrottleRequests.php\",\"line\":59,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Routing\\\\Middleware\\\\ThrottleRequests\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":687,\"function\":\"then\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":662,\"function\":\"runRouteWithinStack\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":628,\"function\":\"runRoute\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Routing\\/Router.php\",\"line\":617,\"function\":\"dispatchToRoute\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":165,\"function\":\"dispatch\",\"class\":\"Illuminate\\\\Routing\\\\Router\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":128,\"function\":\"Illuminate\\\\Foundation\\\\Http\\\\{closure}\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Kernel\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\TransformsRequest\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/TransformsRequest.php\",\"line\":21,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\TransformsRequest\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/ValidatePostSize.php\",\"line\":27,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\ValidatePostSize\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Middleware\\/CheckForMaintenanceMode.php\",\"line\":63,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Middleware\\\\CheckForMaintenanceMode\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fruitcake\\/laravel-cors\\/src\\/HandleCors.php\",\"line\":37,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Fruitcake\\\\Cors\\\\HandleCors\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/fideloper\\/proxy\\/src\\/TrustProxies.php\",\"line\":57,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":167,\"function\":\"handle\",\"class\":\"Fideloper\\\\Proxy\\\\TrustProxies\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Pipeline\\/Pipeline.php\",\"line\":103,\"function\":\"Illuminate\\\\Pipeline\\\\{closure}\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":140,\"function\":\"then\",\"class\":\"Illuminate\\\\Pipeline\\\\Pipeline\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/vendor\\/laravel\\/framework\\/src\\/Illuminate\\/Foundation\\/Http\\/Kernel.php\",\"line\":109,\"function\":\"sendRequestThroughRouter\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Kernel\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"function\":\"handle\",\"class\":\"Illuminate\\\\Foundation\\\\Http\\\\Kernel\",\"type\":\"->\"},{\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/server.php\",\"line\":21,\"function\":\"require_once\"}]},\"duration\":267,\"memory\":4,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:17'),(54,'9155b00a-ca6f-4769-bff2-db99467a0644','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":6,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(55,'9155b00a-cca9-450f-8b0d-63d53cb060db','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba:timer\",\"value\":1597972121,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(56,'9155b00a-cd0b-4cca-a28f-6ddcf659605e','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":6,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(57,'9155b00a-d35c-45cb-9fef-1b169b1569f6','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.71\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(58,'9155b00a-d4de-456c-a839-283907c6be8a','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_oauth_access_tokens` where `id` = \'0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718\' limit 1\",\"time\":\"0.50\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/public\\/index.php\",\"line\":55,\"hash\":\"582ea40f06a9fc81704f64fd36e4bbe6\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(59,'9155b00a-d61b-44dc-b58f-5ae7d176c1f9','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_foods`\",\"time\":\"0.55\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"bde3738a6964c94d8cc4775726c92838\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(60,'9155b00a-d858-4e80-a999-d303b1bd44db','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_measures` where `rl_measures`.`id` in (2)\",\"time\":\"0.72\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"a58b077720f6a7f6a0e125f2efe5f588\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(61,'9155b00a-d9e8-4c9b-b7fd-696867053eca','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_product_presentations` where `rl_product_presentations`.`id` in (4)\",\"time\":\"0.47\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"c57895df500bcf1e63528d6151af9593\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(62,'9155b00a-ec40-4633-827a-2570fb0c8787','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_carbohydrates` where `rl_carbohydrates`.`food_id` in (1)\",\"time\":\"41.63\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"038238f7295ef6530357238d9de1d6bb\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(63,'9155b00a-efcc-4b0f-8bc6-6677bb316e7a','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_fats` where `rl_fats`.`food_id` in (1)\",\"time\":\"0.59\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"27fa7bd3d8ae769fda347c842fc8a3c3\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(64,'9155b00a-f1a0-4618-903d-3274b8f93748','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_proteins` where `rl_proteins`.`food_id` in (1)\",\"time\":\"0.66\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"4cebfc66e06d8f4c6bb2dd81bf04a32e\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(65,'9155b00a-f44a-4d49-ae3b-5d021c4d1a02','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_comments` where `rl_comments`.`food_id` in (1)\",\"time\":\"0.66\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"cc3c7f15a8f0f451db2c70cf6937c8a8\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(66,'9155b00a-f5da-4810-ac5f-57146c0a93ab','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_food_galleries` where `rl_food_galleries`.`food_id` in (1)\",\"time\":\"0.57\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"b55588dfa58f8a4272979133e43a5da3\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(67,'9155b00b-0255-49f3-9abf-29fc6b55c476','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'query','{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from `rl_food_pdfs` where `rl_food_pdfs`.`food_id` in (1)\",\"time\":\"28.34\",\"slow\":false,\"file\":\"\\/Users\\/edgroman\\/Documents\\/Projects\\/rapidos_y_ligeros\\/app\\/Http\\/Controllers\\/Api\\/FoodController.php\",\"line\":27,\"hash\":\"f0bf07f7be889aee881d22942a27edb4\",\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(68,'9155b00b-04c1-40f5-b35b-7bf7857d6e8f','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'cache','{\"type\":\"hit\",\"key\":\"5c785c036466adea360111aa28563bfd556b5fba\",\"value\":7,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37'),(69,'9155b00b-055b-4771-ae77-09066cae2a97','9155b00b-066b-49ae-abcb-47021b23f034',NULL,1,'request','{\"ip_address\":\"127.0.0.1\",\"uri\":\"\\/api\\/foods\",\"method\":\"GET\",\"controller_action\":\"App\\\\Http\\\\Controllers\\\\Api\\\\FoodController@index\",\"middleware\":[\"api\",\"authorizedClient\"],\"headers\":{\"accept\":\"application\\/json\",\"authorization\":\"********\",\"user-agent\":\"PostmanRuntime\\/7.26.3\",\"cache-control\":\"no-cache\",\"postman-token\":\"2442bf8d-fa4d-4f36-8b08-e1904f295b40\",\"host\":\"localhost:8000\",\"accept-encoding\":\"gzip, deflate, br\",\"connection\":\"keep-alive\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"success\":true,\"benchmark\":{\"memory\":\"20.02MB\",\"time\":0.35773587226867676},\"data\":[{\"id\":1,\"name\":\"Tapa De Huevo A La Albahaca\",\"price\":8,\"box_price\":0,\"diet_name\":\"Tapa De Huevo a la Albahaca 1 Pieza con 1 Piezas\",\"package_number\":1,\"piece_package\":4,\"kcal\":220,\"portion\":\"1\",\"portion_nutricionsas\":\"230\",\"image\":\"img_alimentos\\/105\\/croppedImg_1001836446.jpeg\",\"nutritional_table_image\":\"info_tables\\/105\\/croppedImg_632727229.png\",\"nutritional_table\":\"nut table\",\"strategies\":\"Si elijo poner este platillo por la ma\\u00f1ana en automatico estare dando a mi paciente una fuente de energ\\u00eca con el equilibrio ideal. Haciendo que comience su d\\u00eda motivado (por la preparaci\\u00f3n) y con energ\\u00eda por dicho contenido.\\r\\nLa preparaci\\u00f3n de el huevo de este platillo es con el huevo hervido con el proposito de brindar al paciente los mejores beneficios que el huevo pudiera aportar:\\r\\nProteinas de alta calidad ya que cuenta con todos los amino\\u00e1cidos esenciales. \\r\\nEl huevo es uno de los alimentos que tiene m\\u00e1s diversidad de vitaminas (A, E, D, B1, B2, B6 y B12). Mejorar nuestro sistema inmmunol\\u00f3gico y aumentar la ingesta de antioxidantes. Adem\\u00e1s, de los minerales como el hierro, selenio, yodo, \\u00e1cido f\\u00f3lico, zinc, entre otros.\\r\\n\\r\\nHaciendo de este platillo el mejor comienzo del d\\u00eca \\r\\n\",\"rules\":\"-Importante tener actividad fis\\u00edca despues de su consumo\\r\\n-No alterar las cantidades\\r\\n-No utilizar el aceite para cocinar el huevo\\r\\n-No incluir la preparaci\\u00f3n en todos los dias (en los 3 men\\u00fas) o d\\u00edas seguidos (ej. lunes,martes, miercoles)\\r\\n-Preferir este platillo por la ma\\u00f1ana por su contenido de hidratos de carbono.\",\"exaggerate\":\"+ de 1 pieza \",\"risks\":\"TAPA DE HUEVO DURO ALBACA\\r\\nIngredientes:\\r\\nPan integral 1 pieza\\r\\nHuevo 1 pieza\\t\\t\\t\\t\\t\\t\\t\\r\\nJitomate 1pieza \\r\\nAceite de c\\u00e1rtamo 1 cdta\\r\\nQueso parmesano 1 cdta \\r\\nAlbaca al gusto \\r\\n\",\"generals\":\"\\r\\n\\r\\n\\u00bfDONDE COMPRAR?\\r\\nJITOMATES DESHIDRATADOS, \\r\\nPuedes adquirirlos en tienda\\r\\n1.\\r\\n2.\\r\\n3.\\r\\nCosto aproximado: $89\\r\\nSe piden como: Tomate deshidratado o Tomate Tostados al Sol\\r\\n(Agregar Presentaci\\u00f3n Imagen)\\r\\n\\r\\n\",\"equivalent\":\"Pan con queso panela (rebanada 30g).Pan con pechuga de pavo 2 rebanadas delgadas. Pan con claras de huevo 2 piezas. \",\"preparation\":\"Preparaci\\u00f3n:\\r\\n\\r\\n1.\\tPon los huevos en una cazuela y c\\u00fabrelos con agua. A\\u00f1ade una pizca de sal para que luego se pelen m\\u00e1s f\\u00e1cilmente. Pon la cazuela en el fuego y desde que el agua comience a hervir, deja cocer el huevo durante 10 minutos pasado ese tiempo, saca los huevos de la cazuela y d\\u00e9jalos enfriar.\\r\\n2.\\tTostar pan (puedes usar tostador o a fuego lento).\\r\\n3.\\tAgrega a una sart\\u00e9n el jitomate finamente picado con el aceite y d\\u00e9jalo cocinar a fuego lento hasta tomar un color oscuro. Y unta en el pan. \\r\\n4.\\tQuita la cascara al huevo y c\\u00f3rtalo a tu gusto, coloc\\u00e1ndolos sobre el pan \\r\\n5.\\tEspolvorea queso parmesano \\r\\n6.\\tColoca albahaca al gusto \",\"keywords\":\"pan integral,tapa,huevo,hervir,salado,tomate,gourmet\",\"valid_fields\":\"macro_grasa,macro_carbo,macro_percents,macro,preparacion,equivalente,generales,riesgos,exagerar,reglas,estrategias,porcion_nutricionsas,porcion,kcal,num_paq,id_cat,nombre_alimento,imagen_tabla_nutri,imagen_alimento,\",\"validated\":1,\"id_nut\":336,\"restaurant_price\":35,\"preparation_time\":\"20 min\",\"difficulty\":1,\"portability\":5,\"nutritionist_name\":\"L.N. Tania Yetzzi Cornejo Prado\",\"product_presentation_id\":4,\"measure_id\":2,\"created_at\":null,\"updated_at\":null,\"measure\":{\"id\":2,\"name\":\"Piezas\",\"created_at\":null,\"updated_at\":null},\"product_presentation\":{\"id\":4,\"name\":\"Pieza\",\"created_at\":null,\"updated_at\":null},\"carbohydrates\":{\"id\":1,\"total_quantity\":40,\"simple\":0,\"complex\":40,\"food_id\":1,\"created_at\":null,\"updated_at\":null},\"fats\":{\"id\":1,\"total_quantity\":25,\"saturated_fats\":20,\"unsaturated_fats\":80,\"food_id\":1,\"created_at\":null,\"updated_at\":null},\"proteins\":null,\"comments\":[],\"gallery\":[],\"pdfs\":[]}]},\"duration\":363,\"memory\":4,\"hostname\":\"WKMUS9337096\"}','2020-08-21 01:08:37');
/*!40000 ALTER TABLE `rl_telescope_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_telescope_entries_tags`
--

DROP TABLE IF EXISTS `rl_telescope_entries_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `rl_telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  KEY `rl_telescope_entries_tags_tag_index` (`tag`),
  CONSTRAINT `rl_telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `rl_telescope_entries` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_telescope_entries_tags`
--

LOCK TABLES `rl_telescope_entries_tags` WRITE;
/*!40000 ALTER TABLE `rl_telescope_entries_tags` DISABLE KEYS */;
INSERT INTO `rl_telescope_entries_tags` VALUES ('9155afbb-3641-47e4-8547-87984e429fe8','Laravel\\Passport\\Token:0ce6bc1fe6e44ecb3e71f17e9ad3803230f51a58b30b01577fa50adb97b79263faa57cbc87ce0718');
/*!40000 ALTER TABLE `rl_telescope_entries_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_telescope_monitoring`
--

DROP TABLE IF EXISTS `rl_telescope_monitoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_telescope_monitoring` (
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_telescope_monitoring`
--

LOCK TABLES `rl_telescope_monitoring` WRITE;
/*!40000 ALTER TABLE `rl_telescope_monitoring` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_telescope_monitoring` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_times`
--

DROP TABLE IF EXISTS `rl_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_times` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_times`
--

LOCK TABLES `rl_times` WRITE;
/*!40000 ALTER TABLE `rl_times` DISABLE KEYS */;
INSERT INTO `rl_times` VALUES (2,'desayuno',1,NULL,NULL),(3,'shot de energía',1,NULL,NULL),(4,'media mañana',1,NULL,NULL),(5,'saciador comida',1,NULL,NULL),(6,'comida',1,NULL,NULL),(7,'media tarde',1,NULL,NULL),(8,'saciador cena',1,NULL,NULL),(9,'antes del gym',1,NULL,NULL),(10,'durante el gym',1,NULL,NULL),(11,'después del gym',1,NULL,NULL),(12,'durante esfuerzo intelectual ',1,NULL,NULL),(13,'cena',1,NULL,NULL);
/*!40000 ALTER TABLE `rl_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_trendings`
--

DROP TABLE IF EXISTS `rl_trendings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_trendings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `food_id` bigint(20) unsigned NOT NULL,
  `id_nut` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rl_trendings_food_id_foreign` (`food_id`),
  CONSTRAINT `rl_trendings_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `rl_foods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_trendings`
--

LOCK TABLES `rl_trendings` WRITE;
/*!40000 ALTER TABLE `rl_trendings` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_trendings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rl_users`
--

DROP TABLE IF EXISTS `rl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rl_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rl_users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rl_users`
--

LOCK TABLES `rl_users` WRITE;
/*!40000 ALTER TABLE `rl_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `rl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'rapidos_y_ligeros'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-20 20:09:44
