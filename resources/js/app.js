/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

Vue.prototype.$url_base = `${process.env.MIX_APP_URL}/`;
require("vue-axios-interceptors");
window.axios = require("axios");
window.intercepted.$on("response:401", data => {
    console.log("Sessión no iniciada");
    window.location.href = `${process.env.MIX_APP_URL}/login`;
});
window.Vuetify = require("vuetify");
Vue.use(Vuetify);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component("navbar", require("./components/dashboard/NavBar.vue").default);
Vue.component(
    "catalogo",
    require("./components/admin/catalogo/Listado.vue").default
);
Vue.component(
    "categorias",
    require("./components/admin/categorias/Listado.vue").default
);
Vue.component(
    "tiempos",
    require("./components/admin/tiempos/Listado.vue").default
);
Vue.component(
    "categoriascombos",
    require("./components/admin/categorias/combos/Listado.vue").default
);
Vue.component(
    "ingredientes",
    require("./components/admin/ingredientes/Listado.vue").default
);
Vue.component(
    "supermercados",
    require("./components/admin/supermercados/Listado.vue").default
);
Vue.component("loading", require("./components/Loading.vue").default);
Vue.component(
    "favorite",
    require("./components/admin/alimento/Favorito.vue").default
);
Vue.component(
    "alimento-create",
    require("./components/admin/alimento/Create.vue").default
);
Vue.component(
    "alimento-edit",
    require("./components/admin/alimento/Edit.vue").default
);
Vue.component(
    "alimento-verificar",
    require("./components/admin/alimento/Verificar.vue").default
);
Vue.component(
    "mis-alimentos",
    require("./components/admin/alimento/Misalimentos.vue").default
);
Vue.component(
    "buscador",
    require("./components/dashboard/Buscador.vue").default
);
Vue.component(
    "listado-resultados-alimentos",
    require("./components/admin/alimento/ResultadosBuscador.vue").default
);
Vue.component(
    "listado-alimentos-favoritos",
    require("./components/admin/alimento/Favoritos.vue").default
);

Vue.component(
    "alimento-nuevos",
    require("./components/admin/alimento/Nuevos.vue").default
);
import Vue from "vue";
import InputTag from "vue-input-tag";
Vue.component("input-tag", InputTag);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app",
    vuetify: new Vuetify()
});
