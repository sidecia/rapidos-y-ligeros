<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use \App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/dev-login/{token}','Auth\LoginController@devlogin')->name('dev-login');
Route::get('/', 'AdminController@index')->name('admin');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::resource('admin/usuarios', 'UsersController');
Route::post('admin/catalogo/comentarioUsuario', 'CatalogoController@guardaComentarioUsuario');
Route::post('admin/catalogo/email', 'CatalogoController@sendEmailPaciente');
Route::resource('admin/catalogo', 'CatalogoController');
Route::resource('admin/categorias/combos', 'CategoriasCombosController');
Route::get('admin/categorias/alimentos/{id}', 'CategoriesController@alimentos');
Route::resource('admin/categorias', 'CategoriesController');
Route::resource('admin/tiempos', 'TiemposController');
Route::resource('admin/ingredientes', 'IngredientesController');
Route::resource('admin/supermercados', 'SupermercadosController');
Route::resource('admin/alimentos/galerias', 'AlimentosGaleriasController');
Route::get('admin/alimentos/verificar', 'AlimentosController@verificar');
Route::get('/admin/alimentos/usuario', 'AlimentosController@misalimentos');
Route::get('/admin/alimentos/favoritos', 'AlimentosController@favoritos');
Route::resource('admin/alimentos', 'AlimentosController');
Route::put('admin/alimentos/loadSecondImage/{id}', 'AlimentosController@loadSecondImage');
Route::put('admin/alimentos/informacionGeneral/{id}', 'AlimentosController@addInformacionGeneral');
Route::put('admin/alimentos/macronutrientes/{id}', 'AlimentosController@addMacronutrientes');
Route::put('admin/alimentos/informacionConsumo/{id}', 'AlimentosController@addInformacionConsumo');
Route::put('admin/alimentos/materialAdicional/{id}', 'AlimentosController@addMaterialAdicional');
Route::put('admin/alimentos/keywords/{id}', 'AlimentosController@addKeywords');
Route::post('admin/alimentos/galeria', 'AlimentosController@addGaleria');
Route::put('admin/alimentos/validar/{id}', 'AlimentosController@validarCampos');
Route::put('admin/alimentos/sendToValidate/{id}', 'AlimentosController@ChangeStatusValidar');
Route::any('admin/search', 'AlimentosController@buscador');
Route::post('/login', 'Auth\LoginController@authenticate');
Route::get('/logout', 'Auth\LoginController@logout');
