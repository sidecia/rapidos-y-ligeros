<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodEquivalentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_equivalent', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('food_id')->unsigned();
            $table->unsignedBigInteger('equivalentfood_id')->unsigned();
            $table->foreign('food_id')->references('id')->on('foods');
            $table->foreign('equivalentfood_id')->references('id')->on('foods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_equivalent');
    }
}
