<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulos', function (Blueprint $table) {
            $table->id();
            $table->string('titulo');
            $table->integer('modulo_id')->nullable();
            $table->string('icono')->nullable();
            $table->boolean('submenu')->default(0);
            $table->string('enlace');
            $table->enum('tipoenlace',['Interno','Externo'])->default('Interno');
            $table->enum('mostrar',['Sí','No'])->default('Sí');
            $table->string('orden');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulos');
    }
}
