<?php

namespace App\Providers;
use App\Modulo;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view){
            $userAuth = NULL;
            $superadmin = false ;
            $role = NULL;
            if (auth()->check()) {
                $userAuth = auth()->user();
                $role = auth()->user()->tipo_usuario;
            } 
            $modulos = Modulo::whereNull("modulo_id")->where("mostrar","Sí")->orderBy('orden','asc')->get();
            $modulos_permitidos = array();
            if(!empty($modulos->toArray())){
                foreach($modulos as $modulo):
                    if($modulo->submenu):
                        $submodulo=Modulo::where("modulo_id",$modulo->id)
                                            ->where("mostrar","Sí")
                                            ->orderBy('orden','asc')->get();
                        $modulo->submodulos=$submodulo->toArray();
                    endif;    
                    if($modulo->permisos==''){
                        $modulos_permitidos[]=$modulo;
                    }else{
                        $permisos=explode(",",$modulo->permisos);
                        if(in_array($role,$permisos)){
                            $modulos_permitidos[]=$modulo;
                        }

                    }
                    
                endforeach;   
            }
            $config=array(
                'appname' => config('app.name', 'Rapidos y Ligeros')
            );
            $view->with(['userAuth'=>$userAuth]);
            $view->with(['modulos'=>$modulos_permitidos]);
            $view->with(['config'=>$config]);
        });
    }
}
