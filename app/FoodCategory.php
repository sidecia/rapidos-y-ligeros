<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Food;

class FoodCategory extends Model
{
    protected $table = 'rl_food_categories';
    protected $fillable    = [
        'name',
        'description',
        'secuencia',
        'video',
    ];

    protected $hidden = [];

    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }

    public function times()
    {
        return $this->belongsToMany(Time::class);
    }
}
