<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FoodIngredient extends Pivot
{
    protected $fillable	= [ 
        'quantity',
        'ingredient_id',
        'ingredient_measure_id',
        'food_id',
    ];

    protected $hidden = [ ];

    public function ingredient_measures()
    {
        return $this->belongsTo(IngredientMeasure::class);
    }
}
