<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $fillable	= [ 
        'name','order'
    ];

    protected $hidden = [ ];

    public function categories()
    {
        return $this->belongsToMany(FoodCategory::class);
    }
}
