<?php

namespace App\Models;;

use Illuminate\Database\Eloquent\Model;

class FoodEquivalent extends Model
{
    protected $fillable	= [ 
        'food_id',
        'equivalentfood_id'
    ];
    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }
}
