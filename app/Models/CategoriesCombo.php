<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriesCombo extends Model
{
    protected $table ="categories_combos";
    public $timestamps = false;
    protected $fillable	= [ 
        'name',
        'sequence'
    ];
}
