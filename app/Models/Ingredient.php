<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable	= [ 
        'name',
        'image',
    ];

    protected $hidden = [ ];

    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }

    public function ingredient_measures()
    {
        return $this->belongsToMany(IngredientMeasure::class);
    }
}
