<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAlimentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();

        return [
            'image' => 'sometimes|required|nullable|mimes:jpg,jpeg,png',
            'nutritional_table_image' => 'sometimes|required||mimes:jpg,jpeg,png',
        ];
    }
}
