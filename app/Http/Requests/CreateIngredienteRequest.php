<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateIngredienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $id = $this->post('id');
        $complemento = "";
        if (!empty($id)) {
            $complemento = ',' . $id . ',id';
        }
        return [
            'image' => 'nullable|mimes:jpg,jpeg,png',
            'name'                =>  'required|unique:ingredients,name' . $complemento
        ];
    }
}
