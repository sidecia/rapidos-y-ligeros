<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAlimentoInformacionGeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('id');;
        $complemento = "";
        if (!empty($id)) {
            $complemento = ',' . $id . ',id';
        }
        return [
            'name'  =>  'required|unique:foods,name' . $complemento,
            'categories' => 'required',
            'package_number' => 'required',
            'product_presentation_id' => 'required|integer',
            'piece_package' => 'required|integer',
            'measure_id' => 'required|integer',
            'kcal' => 'required|numeric',
            'portion' => 'required',
            'portion_nutricionsas' => 'required',
            'diet_name' => 'required',
            'equivalents' => 'required',
            'ingredients' => 'required',
            'price' => 'required|numeric',
            'box_price' => 'sometimes|required|numeric',
            'restaurant_price' => 'sometimes|required|numeric',
            'preparation_time' => 'required',
            'difficulty' => 'required|integer',
            'portability' => 'required|integer',
            'supermarkets' => 'required'
        ];
    }
}
