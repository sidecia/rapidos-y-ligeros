<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategorieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $id = $this->post('id');
        $complemento = "";
        if (!empty($id)) {
            $complemento = ',' . $id . ',id';
        }
        //dd($complemento);
        return [
            'video' => 'nullable|mimes:mp4,mov,avi,flv',
            'name'                =>  'required|unique:categories,name' . $complemento,
            'description'                =>  'required'
        ];
    }
}
