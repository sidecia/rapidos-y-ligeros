<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $id = $this->post('id');
        $complemento="";
        if(!empty($id)){
            $complemento = ','.$id.',id';
        }
        return [
            'name'                =>  'required|unique:times,name'.$complemento,
            'order'                =>  'required|numeric'
        ];
        
    }
}
