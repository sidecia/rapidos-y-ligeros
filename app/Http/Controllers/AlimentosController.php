<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Models\Fat;
use App\Models\Food;
use App\Models\FoodPdf;
use App\Models\Measure;
use App\Models\Protein;
use App\Models\FoodVideo;
use App\Models\Ingredient;
use App\Models\FoodGallery;
use App\Models\Supermarket;
use App\Models\Carbohydrate;
use App\Models\FoodCategory;
use Illuminate\Http\Request;
use App\Models\FoodEquivalent;
use App\Models\IngredientMeasure;
use App\Models\ProductPresentation;
use App\Models\Favorite;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateAlimentoRequest;
use App\Http\Requests\CreateAlimentoInformacionConsumoRequest;
use App\Http\Requests\CreateAlimentoInformacionGeneralRequest;
use App\Http\Requests\CreateAlimentoInformacionAdicionalRequest;
use PhpParser\Node\Expr\Cast\Object_;

\DB::enableQueryLog();
class AlimentosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:nutriologa,admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = FoodCategory::select('id', 'name')->orderBy('name')->get();
        $foods = Food::select('id', 'name')->where('validated', 1)->orderBy('name')->get();
        $ingredients = Ingredient::select('id', 'name')->orderBy('name')->get();
        $ingredients_measures = IngredientMeasure::select('id', 'measure')->orderBy('measure')->get();
        $measures = Measure::select('id', 'name')->orderBy('name')->get();
        $product_presentation = ProductPresentation::select('id', 'name')->orderBy('name')->get();
        $supermarkets = Supermarket::select('id', 'name', 'image')->orderBy('name')->get();
        return view('admin.alimentos.index', compact('categories', 'foods', 'ingredients', 'measures', 'product_presentation', 'supermarkets', 'ingredients_measures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAlimentoRequest $request)
    {

        $imageName = 'alim_' . time() . '.' . $request->image->extension();
        $food = new Food();
        $last_insert_id = $food->create(['image' => $imageName, 'id_nut' => Auth::id()]);
        $request->image->move(public_path('img_alimentos/' . $last_insert_id->id), $imageName);
        $alimento = Food::findOrFail($last_insert_id->id);
        $alimento->image = 'img_alimentos/' . $last_insert_id->id . '/' . $imageName;
        $alimento->save();
        return response()->json(array('success' => true, 'last_insert_id' => $last_insert_id->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $food = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'alimentosequivalent'
        ])->where('id', $id);
        $foods = $food->get()->toArray();
        return response()->success($foods);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $foodData = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'alimentosequivalent',
        ])->where('id', $id)->get();

        $dataExtra["isOwner"] = $this->isOwner($foodData[0]->id_nut, Auth::id());
        $dataExtra["isAdmin"] = Auth::user()->administrador == 1 ? true : false;
        $categories = FoodCategory::select('id', 'name')->orderBy('name')->get();
        $foods = Food::select('id', 'name')->where('validated', 1)->orderBy('name')->get();
        $ingredients = Ingredient::select('id', 'name')->orderBy('name')->get();
        $ingredients_measures = IngredientMeasure::select('id', 'measure')->orderBy('measure')->get();
        $measures = Measure::select('id', 'name')->orderBy('name')->get();
        $product_presentation = ProductPresentation::select('id', 'name')->orderBy('name')->get();
        $supermarkets = Supermarket::select('id', 'name', 'image')->orderBy('name')->get();
        $usuario = User::select('nombre')->where('id', $foodData[0]->id_nut)->get();

        return view('admin.alimentos.editar', compact('foodData', 'categories', 'foods', 'ingredients', 'measures', 'product_presentation', 'supermarkets', 'ingredients_measures', 'dataExtra', 'usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateAlimentoRequest $request, $id)
    {
        $imageName = 'alim_' . time() . '.' . $request->image->extension();
        $request->image->move(public_path('img_alimentos/' . $id), $imageName);
        $alimento = Food::findOrFail($id);
        $alimento->image = 'img_alimentos/' . $id . '/' . $imageName;
        $alimento->save();
        return response()->json(array('success' => true), 200);
    }

    public function loadSecondImage(CreateAlimentoRequest $request, $id)
    {
        $imageName = 'info_' . time() . '.' . $request->nutritional_table_image->extension();
        $request->nutritional_table_image->move(public_path('info_tables/' . $id), $imageName);
        $alimento = Food::findOrFail($id);
        $alimento->nutritional_table_image = 'info_tables/' . $id . '/' . $imageName;
        $alimento->save();
        return response()->json(array('success' => true), 200);
    }
    public function addInformacionGeneral(CreateAlimentoInformacionGeneralRequest $request, $id)
    {

        $food = Food::findOrFail($id);

        $ids_categories = [];
        foreach ($request->categories as $category) {
            $ids_categories[] = $category["id"];
        }
        $ids_foods = [];

        foreach ($request->equivalents as $foods) {
            $ids_foods[] = $foods["id"];
        }

        $food->update($request->all());
        $food->food_categories()->sync($ids_categories);
        $food->save();
        $food->alimentosequivalent()->sync($ids_foods);
        $food->supermarkets()->sync($request->supermarkets);
        $food->save();
        if (!empty($request->addIngredients)) {
            $ingredients = [];
            foreach ($request->addIngredients as $key => $ingredient) {
                $ingredients[$key]["quantity"] = $ingredient["ingrediente_cantidad"];
                $ingredients[$key]["ingredient_id"] = $ingredient["id"];
                $ingredients[$key]["ingredient_measure_id"] = $ingredient["ingrediente_id_measure"]["id"];
            }
            $food->ingredients()->sync($ingredients);
        }
        return response()->json(array('success' => true), 200);
    }
    public function addMacronutrientes(Request $request, $id)
    {
        $food = Food::findOrFail($id);
        $food->proteins()->delete();
        $food->carbohydrates()->delete();
        $food->fats()->delete();

        if ($request->proteinas["contiene"]) {
            $proteins = new Protein(['total_quantity' => $request->proteinas["proteinas"]]);
            $food->proteins()->save($proteins);
        }

        if ($request->carbohidratos["contiene"]) {
            $arrCarbohidratos = [];

            $arrCarbohidratos["simple"] = $request->carbohidratos["simples"];
            $arrCarbohidratos["complex"] = $request->carbohidratos["complejos"];

            $arrCarbohidratos["total_quantity"] = $request->carbohidratos["total"];
            $carbohidratos = new Carbohydrate($arrCarbohidratos);
            $food->carbohydrates()->save($carbohidratos);
        }

        if ($request->grasas["contiene"]) {
            $arrGrasas = [];

            $arrGrasas["saturated_fats"] = $request->grasas["saturadas"];
            $arrGrasas["unsaturated_fats"] = $request->grasas["insaturadas"];

            $arrGrasas["total_quantity"] = $request->grasas["total"];
            $grasas = new Fat($arrGrasas);
            $food->fats()->save($grasas);
        }
        $food->save();
    }
    public function addInformacionConsumo(CreateAlimentoInformacionConsumoRequest $request, $id)
    {
        $food = Food::findOrFail($id);
        $food->update($request->all());
        $food->save();
        return response()->json(array('success' => true), 200);
    }
    public function verificar(Request $request)
    {

        $nutriologas = User::whereHas('foods', function ($query) {
            $query->where('name', '!=', '')->where('validated', 0)->orWhereNull('validated')->where('valid_fields', '=', '')->orWhereNull('valid_fields');
        })->with(['foods' => function ($query) {
            $query->where('name', '!=', '')->where('validated', 0)->orWhereNull('validated')->where('valid_fields', '=', '')->orWhereNull('valid_fields');
        }])->where('status', 'activo')->get();

        foreach ($nutriologas as $nut) {

            $foods = $nut->foods;
            $nut->foods_count = count($nut->foods);
            foreach ($foods as $food) {
                $food_id = $food->id;
                $food_real = Food::with([
                    'food_categories', 'ingredients',
                    'videos',
                    'supermarkets',
                    'measure',
                    'product_presentation',
                    'carbohydrates',
                    'fats',
                    'proteins',
                    'comments',
                    'gallery',
                    'pdfs'
                ])->where('id', $food_id)->get();
                $food->food_categories = $food_real[0]->food_categories;
                $food->ingredients = $food_real[0]->ingredients;
                $food->videos = $food_real[0]->videos;
                $food->supermarkets = $food_real[0]->supermarkets;
                $food->gallery = $food_real[0]->gallery;
                $food->pdfs = $food_real[0]->pdfs;
                $food->url = url("admin/alimentos/" . $food_id . "/edit");
            }
        }
        $lastfoodsten = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'nut_favorites'
        ])->where('validated', 0)->where('created_at', '>', Carbon::now()->subDays(10)->toDateTimeString())->orderBy('created_at', 'DESC')->get();
        foreach ($lastfoodsten as $key => $value) {
            if ($value->send_valid_date != "" && $value->send_valid_date != null) {
                Carbon::setLocale('es');
                $value->diffDateSendValidate = Carbon::parse($value->send_valid_date)->diffForHumans();
            }
        }
        $rezagadosfoods = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'nut_favorites'
        ])->where('validated', 0)->where('created_at', '<', Carbon::now()->subDays(10))->orderBy('created_at', 'DESC')->get();
        foreach ($rezagadosfoods as $key => $value) {
            if ($value->send_valid_date != "" && $value->send_valid_date != null) {
                Carbon::setLocale('es');
                $value->diffDateSendValidate = Carbon::parse($value->send_valid_date)->diffForHumans();
            }
        }
        return view('admin.alimentos.verificar', compact("nutriologas", "lastfoodsten", "rezagadosfoods"));
    }
    public function addKeywords(CreateAlimentoRequest $request, $id)
    {
        $food = Food::findOrFail($id);
        if ($request->keywords != '') {
            $keywords = implode(',', $request->keywords);
            $food->keywords = $keywords;
            $food->save();
        } else {
            $food->keywords = '';
            $food->save();
        }

        return response()->json(array('success' => true), 200);
    }
    public function addMaterialAdicional(CreateAlimentoInformacionAdicionalRequest $request, $id)
    {
        $food = Food::findOrFail($id);
        $food->pdfs()->delete();
        $food->videos()->delete();
        if (!empty($request->pdfs)) {
            $url_pdfs = array();
            foreach ($request->pdfs as $pdf) {
                $url_pdfs[] = new FoodPdf(array('url_pdf' => $pdf));
            }
            $food->pdfs()->saveMany($url_pdfs);
        }

        if (!empty($request->videos)) {
            foreach ($request->videos as $video) {
                $url_videos[] = new FoodVideo(array('url_video' => $video));
            }
            $food->videos()->saveMany($url_videos);
        }

        return response()->json(array('success' => true), 200);
    }
    public function addGaleria(CreateAlimentoRequest $request)
    {
        $imageName = 'galeria_' . time() . uniqid() . '.' . $request->image->extension();
        $request->image->move(public_path('galerias/' . $request->id), $imageName);
        $alimento = Food::findOrFail($request->id);
        $img = 'galerias/' . $request->id . '/' . $imageName;
        $galeria = new FoodGallery(['image' => $img, 'food_id' => $request->id]);
        $last_insert_id = $alimento->gallery()->save($galeria);
        return response()->json(array('success' => true, "nameImage" => $img, "id" => $last_insert_id), 200);
    }
    public function misalimentos(Request $request)
    {
        $id_nut =  Auth::id();
        $categorias = FoodCategory::whereHas('foods', function ($query) use ($id_nut) {
            $query->where('id_nut', $id_nut);
        })->with([
            'foods' => function ($query) use ($id_nut) {
                $query->where('id_nut', $id_nut);
            },
            'foods.food_categories',
            'foods.ingredients',
            'foods.videos',
            'foods.supermarkets',
            'foods.measure',
            'foods.product_presentation',
            'foods.carbohydrates',
            'foods.fats',
            'foods.proteins',
            'foods.comments',
            'foods.gallery',
            'foods.pdfs',
            'foods.nut_favorites'
        ])->get();
        $uncategorized = Food::doesntHave('food_categories')->with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'nut_favorites'
        ])->where('id_nut', $id_nut)->get();
        $sinCategoria = collect(['id' => 0, 'name' => 'Sin categoría', 'description' => '', 'foods' => $uncategorized]);

        $lastFoodsPend = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'nut_favorites'
            //])->where('id_nut', $id_nut)->where('validated', 0)->orWhere('valid_fields', '=', '')->orWhereNull('valid_fields')->orderBy('created_at', 'desc')->limit(6)->toSql();
        ])->where('id_nut', $id_nut)->where(function ($query) {
            $query->orWhere('validated', 0)
                ->orWhere('validated', 2);
        })->where(function ($query) {
            $query->orWhere('valid_fields', '=', '')
                ->orWhereNull('valid_fields');
        })->orderBy('created_at', 'desc')->limit(6)->get();

        $lastFoodsValid = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'nut_favorites'
        ])->where('id_nut', $id_nut)->where('validated', 1)->orderBy('created_at', 'desc')->limit(6)->get();

        return view('admin.alimentos.usuario', compact("categorias", "lastFoodsValid", "lastFoodsPend", 'sinCategoria'));
    }
    public function favoritos(Request $request)
    {
        $id_nut =  Auth::id();
        $buscar = "especias";
        $foods = Food::whereHas('nut_favorites', function ($query) use ($id_nut) {
            $query->where('id_nut', $id_nut);
        })->with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'nut_favorites'
        ])->orderBy('name')->get();
        return view('admin.alimentos.favoritos', compact("foods", "buscar"));
    }
    public function buscador(Request $request)
    {

        $buscar = $request->input('q');
        if (!empty($buscar)) {
            $foods = Food::whereHas('ingredients', function ($query) use ($buscar) {
                $query->where('name', 'like', '%' . $buscar . '%');
            })->with([
                'ingredients' => function ($query) use ($buscar) {
                    $query->where('name', 'LIKE', '%' . $buscar . '%');
                },
                'videos',
                'supermarkets',
                'measure',
                'product_presentation',
                'carbohydrates',
                'fats',
                'proteins',
                'comments',
                'gallery',
                'pdfs',
                'food_categories',
                'nut_favorites'
            ])->orWhere('name', 'LIKE', '%' . $buscar . '%')->get();
        } else {
            $foods = [];
        }
        return view('admin.alimentos.buscador', compact("foods", "buscar"));
    }
    public function validarCampos(Request $request, $id)
    {
        $fields = explode(",", $request->campos_validados);
        $clear_fields = array_filter($fields);
        $new_fields = implode(",", $clear_fields);
        $food = Food::findOrFail($id);
        if (count($fields) == $request->total) {
            $food->validated = 1;
            $food->valid_date = Carbon::now();
        } else {
            $food->validated = 0;
        }
        $food->valid_fields = $new_fields;
        $food->save();
        return response()->json(array('success' => true), 200);
    }
    public function ChangeStatusValidar(Request $request, $id)
    {
        $food = Food::findOrFail($id);
        $food->send_valid_date = Carbon::now();
        $food->validated = 0;
        $food->save();
        return response()->json(array('success' => true), 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $food = Food::findOrFail($id);
        $food->delete();
        return response()->json(array('success' => true), 200);
    }

    private function isOwner($idNut, $idUser)
    {
        if ($idNut == $idUser) {
            return true;
        } else {
            return false;
        }
    }
}
