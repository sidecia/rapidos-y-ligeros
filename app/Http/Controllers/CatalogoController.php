<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\AlimentoInfPaciente;
use App\Models\FoodCategory;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Resources\CategorieCollection;
use App\Http\Resources\FoodCollection;
use App\Models\Food;
use Auth;

class CatalogoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:nutriologa,admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = FoodCategory::orderBy('name', 'asc')->get();

        return view('admin.catalogo.index', compact("categorias"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function sendEmailPaciente(Request $request)
    {
        $food = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories'
        ]);
        $food->where('id', $request->id);
        $food->where('validated', 1);
        $food->get();
        $foods = $food->get();
        $userAuth = auth()->user();

        $infomail = (object)[];
        $infomail->emailTo = $request->mailPaciente;
        $infomail->comentario = $request->comentarioAlimento ? $request->comentarioAlimento : "";

        $mail_send = Mail::to($request->mailPaciente)->send(new AlimentoInfPaciente($foods, $userAuth, $infomail));

        return response()->json(array('success' => true, 'emailSend' => 1), 200);
    }
    public function guardaComentarioUsuario(Request $request)
    {
        $comentario = new Comment();
        $user_id = Auth::user()->id;
        $request->merge(['id_nut' =>  $user_id]);
        $last_insert_id = $comentario->create($request->all());
        return response()->json(array('success' => true, 'last_insert_id' => $last_insert_id->id), 200);
    }
}
