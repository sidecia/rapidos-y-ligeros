<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Models\Fat;
use App\Models\Food;
use App\Models\FoodPdf;
use App\Models\Measure;
use App\Models\Protein;
use App\Models\FoodVideo;
use App\Models\Ingredient;
use App\Models\FoodGallery;
use App\Models\Supermarket;
use App\Models\Carbohydrate;
use App\Models\FoodCategory;
use Illuminate\Http\Request;
use App\Models\FoodEquivalent;
use App\Models\IngredientMeasure;
use App\Models\ProductPresentation;
use App\Models\Favorite;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Null_;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $role = auth()->user()->tipo_usuario;
        $id_nut =  Auth::id();
        $alimentos = Food::latest()->with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories',
            'nut_favorites'
        ])->orderBy('valid_date', 'DESC')->orderBy('id', 'DESC')->where('validated', 1)->take(10)->get();

        $lastFoodsPend = Food::where('id_nut', $id_nut)->where(function ($query) {
            $query->orWhere('validated', 0)
                ->orWhere('validated', 2);
        })->where(function ($query) {
            $query->orWhere('valid_fields', '=', '')
                ->orWhereNull('valid_fields');
        })->orderBy('created_at', 'desc')->limit(12)->get();
        foreach ($lastFoodsPend as $key => $value) {
            if ($value->send_valid_date != "" && $value->send_valid_date != null) {
                Carbon::setLocale('es');
                $value->diffDateSendValidate = Carbon::parse($value->send_valid_date)->diffForHumans();
            }
        }
        $foodincomplete = Food::where('id_nut', $id_nut)->where('validated', NULL)->orderBy('created_at', 'desc')->limit(12)->get();

        return view('admin', compact("alimentos", "lastFoodsPend", "foodincomplete"));
    }
}
