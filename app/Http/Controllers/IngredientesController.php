<?php

namespace App\Http\Controllers;

use App\Models\Ingredient;
use Illuminate\Http\Request;
use App\Http\Resources\IngredienteCollection;
use App\Http\Requests\CreateIngredienteRequest;

class IngredientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:nutriologa,admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ingredientes = Ingredient::orderBy('name', 'asc')->get();


        if ($request->input("ajax"))
            return new IngredienteCollection($ingredientes);

        return view('admin.ingredientes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateIngredienteRequest $request)
    {
        if (isset($request->image) && $request->image != 'undefined') {
            $imageName = 'ingre_' . time() . uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('ingredientes'), $imageName);
        } else {
            $imageName = NULL;
        }
        $img = 'ingredientes/' . $imageName;
        $ingrediente = new Ingredient();
        $last_insert_id = $ingrediente->create(["name" => $request["name"], "image" => $img]);
        return response()->json(array('success' => true, 'last_insert_id' => $last_insert_id->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateIngredienteRequest $request, $id)
    {
        if (isset($request->image)) {
            $imageName = 'ingre_' . time() . uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('ingredientes'), $imageName);
        } else {
            $imageName = NULL;
        }
        $img = 'ingredientes/' . $imageName;
        $ingrediente = Ingredient::findOrFail($id);
        $ingrediente->name = $request["name"];
        $ingrediente->image = $img;
        $ingrediente->save();
        return response()->json(array('success' => true), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
