<?php

namespace App\Http\Controllers;

use App\Models\Supermarket;
use Illuminate\Http\Request;
use App\Http\Resources\SupermercadoCollection;
use App\Http\Requests\CreateSupermercadoRequest;

class SupermercadosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:nutriologa,admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $supermercados = Supermarket::orderBy('name', 'asc')->get();
        if ($request->input("ajax"))
            return new SupermercadoCollection($supermercados);

        return view('admin.supermercados.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSupermercadoRequest $request)
    {
        if (isset($request->image)) {
            $imageName = 'super_' . time() . uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('supers'), $imageName);
        } else {
            $imageName = NULL;
        }
        $img = 'supers/' . $imageName;
        $supermercado = new Supermarket();
        $last_insert_id = $supermercado->create(["name" => $request["name"], "image" => $img]);
        return response()->json(array('success' => true, 'last_insert_id' => $last_insert_id->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateSupermercadoRequest $request, $id)
    {
        if (isset($request->image)) {
            $imageName = 'super_' . time() . uniqid() . '.' . $request->image->extension();
            $request->image->move(public_path('supers'), $imageName);
        } else {
            $imageName = NULL;
        }
        $img = 'supers/' . $imageName;
        $supermercado = Supermarket::findOrFail($id);
        $supermercado->name = $request["name"];
        $supermercado->image = $img;
        $supermercado->save();
        return response()->json(array('success' => true), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
