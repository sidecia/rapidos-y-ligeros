<?php

namespace App\Http\Controllers\Api;

use App\Models\Food;
use App\Models\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FoodIngredient;
use App\Models\IngredientMeasure;
use Illuminate\Support\Facades\Validator;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foods = Food::with([
            'ingredients',
            'videos',
            'supermarkets',
            'measure',
            'product_presentation',
            'carbohydrates',
            'fats',
            'proteins',
            'comments',
            'gallery',
            'pdfs',
            'food_categories'
        ])->where('validated',1)->get();

        return response()->success($foods);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {        
        $food_category = $request->food_category;
        if($food_category != '') {
            $food = Food::whereHas('food_categories', function($q) use($food_category) {
                $q->where('food_category_id', $food_category);
            })->with([
                'ingredients',
                'videos',
                'supermarkets',
                'measure',
                'product_presentation',
                'carbohydrates',
                'fats',
                'proteins',
                'comments',
                'gallery',
                'pdfs',
                'food_categories',
                'nut_favorites',
            ]);
        } else {
            $food = Food::with([
                'ingredients',
                'videos',
                'supermarkets',
                'measure',
                'product_presentation',
                'carbohydrates',
                'fats',
                'proteins',
                'comments',
                'gallery',
                'pdfs',
                'food_categories'
            ]);
        }

        if($request->id) $food->where('id',$request->id);
        if($request->nut) $food->where('id_nut',$request->nut);
        $food->where('validated',1);
        //if($request->fav) $food->where('favorites.id_nut',$request->nut);

        /*$food = Food::whereHas('nut_favorites', function ($query) use ($id_nut) {
            $query->where('id_nut', $id_nut);
        })->get();*/

        $foods = $food->get()->toArray();

        foreach($foods as $key_food => $food) {
            foreach($food['ingredients'] as $key_ingredient => $ingredient) {
                $measure = IngredientMeasure::where('id',$ingredient['pivot']['ingredient_measure_id'])->first();

                $foods[$key_food]['ingredients'][$key_ingredient]['pivot']['measure'] = $measure->measure;
            }            
        }

        return response()->success($foods);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function favorites(Request $request)
    {
        $id_nut = $request->nut;
        $food_category = $request->food_category;

        $favorites = Favorite::whereHas('food', function($q) use ($food_category) {
            $q->whereHas('food_categories', function($q) use ($food_category) {
                $q->where('food_category_id', $food_category);
            });

            $q->where('validated', 1);

        })->with(
            'food',
            'food.ingredients',
            'food.videos',
            'food.supermarkets',
            'food.measure',
            'food.product_presentation',
            'food.carbohydrates',
            'food.fats',
            'food.proteins',
            'food.comments',
            'food.gallery',
            'food.pdfs',
            'food.food_categories'
        );

        $favorites->where('id_nut',$id_nut);

        return response()->success($favorites->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function assign_favorite(Request $request)
    {
		$validator	= Validator::make($request->all(), [
			'nut' => 'required',
			'food' => 'required|exists:foods,id'
        ]);

        if( $validator->fails() ){	return response()->error($validator->messages()); }

        $favorite = Favorite::where(['id_nut' => $request->nut, 'food_id' => $request->food])->first();

        if($favorite) return response()->success(['message' => 'Already user favorite.']);
        
        $food = Food::find($request->food);        
        
        $food->nut_favorites()->create(['id_nut' => $request->nut]);

        return response()->success(['message' => 'User favorite assigned.']);
    }


    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete_favorite(Request $request)
    {
		$validator	= Validator::make($request->all(), [
			'nut' => 'required',
			'food' => 'required|exists:foods,id'
        ]);

        if( $validator->fails() ){	return response()->error($validator->messages()); }

        $favorite = Favorite::where(['id_nut' => $request->nut, 'food_id' => $request->food])->pluck('id');

        Favorite::destroy($favorite);

        return response()->success(['message' => 'User favorite deleted.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
