<?php

namespace App\Http\Controllers;

use App\Models\FoodCategory;
use App\Models\Time;
use App\Models\Favorite;
use Illuminate\Http\Request;
use App\Http\Resources\CategorieCollection;
use App\Http\Requests\CreateCategorieRequest;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:nutriologa,admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categorias = FoodCategory::with(['times', 'foods'])->orderBy('secuencia', 'desc')->get();

        foreach ($categorias as $categoria) {
            $categoria->stringTimes = $categoria->times->pluck('name')->implode(',');
            $categoria->totalFoods = count($categoria->foods);
        }

        if ($request->input("ajax"))
            return new CategorieCollection($categorias);

        $times = Time::orderBy('order')->get();

        return view('admin.categorias.index', compact('times'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategorieRequest $request)
    {
        if (isset($request->video)) {
            $videoName = 'catvideo_' . time() . uniqid() . '.' . $request->video->extension();
            $request->video->move(public_path('videos_categorias'), $videoName);
        } else {
            $videoName = NULL;
        }
        $video = 'videos_categorias/' . $videoName;

        $categoria = new FoodCategory();
        $category_id = $categoria->create(["name" => $request["name"], "description" => $request["description"], "video" => $video, "secuencia" => $request["secuencia"]]);

        $times = json_decode($request->times);
        $ids_times = [];
        foreach ($times as $time) {
            $ids_times[] = $time->id;
        }
        $categoria_2 = FoodCategory::find($category_id->id);
        $categoria_2->times()->attach($ids_times);
        $categoria_2->save();
        return response()->json(array('success' => true, 'last_insert_id' => $category_id->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCategorieRequest $request, $id)
    {
        if (isset($request->video)) {
            $videoName = 'catvideo_' . time() . uniqid() . '.' . $request->video->extension();
            $request->video->move(public_path('videos_categorias'), $videoName);
        } else {
            $videoName = NULL;
        }
        $video = 'videos_categorias/' . $videoName;

        $categoria = FoodCategory::findOrFail($id);
        $times = json_decode($request->times);
        $ids_times = [];
        foreach ($times as $time) {
            $ids_times[] = $time->id;
        }
        $categoria->times()->sync($ids_times);
        $categoria->name = $request["name"];
        $categoria->description = $request["description"];
        $categoria->secuencia = $request["secuencia"];
        $categoria->video = $video;
        $categoria->save();

        return response()->json(array('success' => true), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function alimentos($id)
    {

        $food_category = FoodCategory::with([
            'foods' => function ($query) {
                $query->orderBy('name', 'asc');
            }, 'foods.nut_favorites' => function ($query) {
                $userAuth = auth()->user();
                $query->where('id_nut', '=', $userAuth->id);
            }, 'foods.food_categories' => function ($query) {
                $query->select('name');
            }

        ])->whereId($id)->first();

        return response()->success($food_category);
    }
}
