<?php

namespace App\Http\Controllers;
use App\Models\Time;
use Illuminate\Http\Request;
use App\Http\Resources\TimeCollection;
use App\Http\Requests\CreateTimeRequest;

class TiemposController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:nutriologa,admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tiempos = Time::orderBy('order','asc')->get();

        
        if($request->input("ajax"))
            return new TimeCollection($tiempos);

        return view('admin.tiempos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTimeRequest $request)
    {
        $tiempo = new Time();
        $last_insert_id = $tiempo->create($request->all());
        return response()->json(array('success' => true, 'last_insert_id' => $last_insert_id->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateTimeRequest $request, $id)
    {
        $tiempo = Time::findOrFail($id);
        $tiempo->update($request->all());
        return response()->json(array('success' => true), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
