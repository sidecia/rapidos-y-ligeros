<?php

namespace App\Http\Controllers;
use App\Models\CategoriesCombo;
use Illuminate\Http\Request;
use App\Http\Resources\CategoriaComboCollection;
use App\Http\Requests\CreateCategoriaComboRequest;

class CategoriasCombosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:nutriologa,admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        $categorias = CategoriesCombo::orderBy('sequence','asc')->get();

        
        if($request->input("ajax"))
            return new CategoriaComboCollection($categorias);

        return view('admin.categorias.combo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoriaComboRequest $request)
    {
        $categoria = new CategoriesCombo();
        $last_insert_id = $categoria->create($request->all());
        return response()->json(array('success' => true, 'last_insert_id' => $last_insert_id->id), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCategoriaComboRequest $request, $id)
    {
        $categoria = CategoriesCombo::findOrFail($id);
        $categoria->update($request->all());
        return response()->json(array('success' => true), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
