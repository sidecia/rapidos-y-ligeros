<?php

namespace App\Http\Controllers;

use App;
use \App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CreateUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $verifier = App::make('validation.presence');

        $verifier->setConnection('mysql2');

        $validator = Validator::make($request->all(), [
            'nombre'         => 'required',
            'usuario'        => 'required|unique:usuarios,usuario',
            'password'       => 'required|confirmed'
        ]);

        $validator->setPresenceVerifier($verifier);

        date_default_timezone_set('America/Mexico_City');
        $fecha_alta = date("Y-m-d H:i:s");
        $request->merge(['fecha_alta' => $fecha_alta]);
        $user = User::create($request->all());
        $user->save();
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::findOrFail($id);

        //$this->authorize('edit',$usuario);


        return view('users.edit', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $verifier = App::make('validation.presence');

        $verifier->setConnection('mysql2');

        $validator = Validator::make($request->all(), [
            'nombre'        => 'required',
            'usuario'       => 'required|unique:usuarios,usuario,' . $this->route('usuario'),
        ]);

        $validator->setPresenceVerifier($verifier);


        $usuario = User::findOrFail($id);

        //$this->authorize('update',$usuario);

        $usuario->update($request->all());

        return back()->with('info', 'Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::findOrFail($id);

        $this->authorize('destroy', $usuario);

        $usuario->delete();

        return back();
    }
    public function AuthRouteAPI(Request $request)
    {
        return $request->user();
    }
}
