<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use \App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return 'usuario';
    }

    public function authenticate(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'usuario' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {

            return redirect()->route('/login')
                ->withErrors($validator);
        } else {

            $user = User::where('correo_personal', $request->usuario)
                ->where('password', $request->password)
                ->where('status', 'activo')
                ->orWhere(
                    function ($query) {
                        $query->where('tipo_usuario', 'admin')
                            ->where('tipo_usuario', 'nutriologa');
                    }
                )
                ->first();

            if (!isset($user)) {
                return redirect()->route('login')->with('global', 'There was a problem logging you in. Please check your credentials and try again.');;
            } else {
                Auth::login($user);
                return redirect()->route('admin');
            }
        }
    }
    public function logout(Request $request)
    {
        $this->guard('web_buyer')->logout();

        return redirect('/');
    }
    public function devlogin($token)
    {

        $encryption_iv = '1234567891011121';
        $encryption_key = "SistemaRapidosyLigerosAthenticacionDev";
        $ciphering = "AES-128-CTR";
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;

        $decryption = openssl_decrypt($token, $ciphering, $encryption_key, $options, $encryption_iv);

        $id_user =  explode("@/", $decryption);

        $user = User::where('id', $id_user[0])
            ->where('status', 'activo')
            ->orWhere(
                function ($query) {
                    $query->where('tipo_usuario', 'admin')
                        ->where('tipo_usuario', 'nutriologa');
                }
            )
            ->first();
        if (!isset($user)) {
            return redirect()->route('login')->with('global', 'There was a problem logging you in. Please check your credentials and try again.');;
        } else {
            Auth::login($user);
            return redirect()->route('admin');
        }
    }
}
