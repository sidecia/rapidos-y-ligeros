<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlimentoInfPaciente extends Mailable
{
    use Queueable, SerializesModels;
    public $foods;
    public $usuario;
    public $infomail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($foods, $usuario,$infomail)
    {
        $this->foods = $foods;
        $this->usuario = $usuario;
        $this->infomail= $infomail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Tu nutrióloga te recomienda este alimento ".$this->foods[0]->name )
        ->view('admin.mails.alimento.paciente');
    }
}
