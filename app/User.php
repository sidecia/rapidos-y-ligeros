<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Food;

class User extends Authenticatable
{

    use Notifiable;

    //protected $table = 'usuarios';
    protected $table = 'usuarios';
    protected $connection = 'mysql2';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*public function setPasswordAttribute($password){

        $this->attributes['password_encriptada'] = bcrypt($password);
        $this->attributes['password'] = $password;
        $this->attributes['password_crypt'] = $password;

    }
    public function getAuthPassword()
    {
        return $this->password_encriptada;
    }*/
    public function getAuthPassword()
    {
        return $this->password;
    }
    public function hasRole($role, $id)
    {
        return User::where('tipo_usuario', '=', $role)->where('id', '=', $id)->get()->count();
    }
    public function foods()
    {
        return $this->hasMany(Food::class, 'id_nut');
    }
}
