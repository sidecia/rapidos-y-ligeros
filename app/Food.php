<?php

namespace App;

use App\FoodCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Food extends Model
{
    use SoftDeletes;
    protected $table = 'rl_foods';
    protected $dates = ['deleted_at'];
    protected $fillable    = [
        'name',
        'price',
        'box_price',
        'diet_name',
        'package_number',
        'piece_package',
        'kcal',
        'portion',
        'portion_nutricionsas',
        'image',
        'nutritional_table',
        'strategies',
        'rules',
        'exaggerate',
        'risks',
        'generals',
        'equivalent',
        'preparation',
        'keywords',
        'valid_fields',
        'validated',
        'id_nut',
        'restaurant_price',
        'preparation_time',
        'difficulty',
        'portability',
        'nutritionist_name',
        'product_presentation_id',
        'measure_id',
    ];

    protected $hidden = [];

    public function food_categories()
    {
        return $this->belongsToMany(FoodCategory::class);
    }
    public function usuario()
    {
        return $this->belongsTo(User::class);
    }
}
